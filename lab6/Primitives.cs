﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6
{
    class Point
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public Point(double x, double y, double z)
        {
            X = x; Y = y; Z = z;
        }

        public Point(Point p)
        {
            X = p.X; Y = p.Y; Z = p.Z;
        }

        public static double Distance(Point p1, Point p2)
        {
            double deltaX = p2.X - p1.X;
            double deltaY = p2.Y - p1.Y;
            double deltaZ = p2.Z - p1.Z;

            return Math.Sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + X.GetHashCode();
                hash = hash * 23 + Y.GetHashCode();
                hash = hash * 23 + Z.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Point other = (Point)obj;
            return X.Equals(other.X) && Y.Equals(other.Y) && Z.Equals(other.Z);
        }
    }

    class Edge
    {
        public Point P1 { get; set; }
        public Point P2 { get; set; }

        public Edge(Edge edge)
        {
            P1 = new Point(edge.P1);
            P2 = new Point(edge.P2);
        }

        public Edge(Point p1, Point p2)
        {
            P1 = p1; P2 = p2;
        }

        public Edge(double x1, double y1, double z1, double x2, double y2, double z2)
        {
            P1 = new Point(x1, y1, z1);
            P2 = new Point(x2, y2, z2);
        }

        public double GetLength()
        {
            return Point.Distance(P1, P2);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + P1.GetHashCode();
                hash = hash * 23 + P2.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Edge other = (Edge)obj;
            return P1.Equals(other.P1) && P2.Equals(other.P2);
        }
    }

    class Polygon
    {
        public List<Edge> Edges { get; set; }

        public Polygon()
        {
            Edges = new List<Edge>();
        }

        public Polygon(List<Edge> edges)
        {
            Edges = edges;
        }

        public void AddEdge(Edge e)
        {
            Edges.Add(e);
        }
    }

    class Polyhedron
    {
        public List<Polygon> Polygons { get; set; }

        public Polyhedron()
        {
            Polygons = new List<Polygon>();
        }

        public Polyhedron(List<Polygon> polygons)
        {
            Polygons = polygons;
        }

        public void AddPolygon(Polygon polygon)
        {
            Polygons.Add(polygon);
        }

        public void Translate(double deltaX, double deltaY, double deltaZ)
        {
            Matrix matr = new Matrix(4, 4, 1,      0,      0,      0,
                                           0,      1,      0,      0,
                                           0,      0,      1,      0,
                                           deltaX, deltaY, deltaZ, 1);
            ApplyMatrixTransform(matr);
        }

        public void Rotate(string axis, double angle)
        {
            Matrix matr = axis == "X" ? 
                GetRotateXMatrix(angle) : axis == "Y" ?
                GetRotateYMatrix(angle) : 
                GetRotateZMatrix(angle);

            ApplyMatrixTransformFromCenter(matr);
        }

        public void RotateAroundLine(Edge line, double angle)
        {
            double length = Point.Distance(line.P1, line.P2);
            double l = (line.P2.X - line.P1.X) / length;
            double m = (line.P2.Y - line.P1.Y) / length;
            double n = (line.P2.Z - line.P1.Z) / length;

            double cosPhi = Utils.Cos(angle);
            double sinPhi = Utils.Sin(angle);

            Matrix matr = new Matrix(4, 4, l * l + cosPhi * (1 - l * l), l * (1 - cosPhi) * m + n * sinPhi, l * (1 - cosPhi) * n - m * sinPhi, 0,
                                           l * (1 - cosPhi) * m - n * sinPhi, m * m + cosPhi * (1 - m * m), m * (1 - cosPhi) * n + l * sinPhi, 0,
                                           l * (1 - cosPhi) * n + m * sinPhi, m * (1 - cosPhi) * n - l * sinPhi, n * n + cosPhi * (1 - n * n), 0,
                                           0, 0, 0, 1);
            ApplyMatrixTransform(matr);
        }

        public void Mirror(string axis)
        {
            ApplyMatrixTransformFromCenter(GetMirrorMatrix(axis));
        }

        public void Scale(double coeff)
        {
            ApplyMatrixTransformFromCenter(GetScaleMatrix(coeff));
        }

        public void ApplyMatrixTransformFromCenter(Matrix transform)
        {
            Point center = GetCenter();

            Translate(-center.X, -center.Y, -center.Z);

            ApplyMatrixTransform(transform);

            Translate(center.X, center.Y, center.Z);
        }

        public void ApplyMatrixTransform(Matrix transform)
        {
            Polygons.ForEach(polygon =>
            {
                polygon.Edges = polygon.Edges.Select(e =>
                {
                    var m1 = Matrix.FromPoint(e.P1);
                    var m2 = Matrix.FromPoint(e.P2);

                    var res1 = m1.GetMultBy(transform);
                    var res2 = m2.GetMultBy(transform);

                    return new Edge(new Point(res1[0, 0], res1[0, 1], res1[0, 2]), new Point(res2[0, 0], res2[0, 1], res2[0, 2]));
                }).ToList();
            });
        }
        
        private Matrix GetRotateXMatrix(double angle)
        {
            return new Matrix(4, 4, 1, 0,                 0,                0,
                                    0, Utils.Cos(angle),  Utils.Sin(angle), 0,
                                    0, -Utils.Sin(angle), Utils.Cos(angle), 0,
                                    0, 0,                 0,                1);
        }

        private Matrix GetRotateYMatrix(double angle)
        {
            return new Matrix(4, 4, Utils.Cos(angle), 0, -Utils.Sin(angle), 0,
                                    0,                1, 0,                 0,
                                    Utils.Sin(angle), 0, Utils.Cos(angle),  0,
                                    0,                0, 0,                 1);
        }

        private Matrix GetRotateZMatrix(double angle)
        {
            return new Matrix(4, 4, Utils.Cos(angle),  Utils.Sin(angle), 0, 0,
                                    -Utils.Sin(angle), Utils.Cos(angle), 0, 0,
                                    0,                 0,                1, 0,
                                    0,                 0,                0, 1);
        }

        private Matrix GetMirrorMatrix(string axis)
        {
            int kx = axis == "X" ? 1 : -1;
            int ky = axis == "Y" ? 1 : -1;
            int kz = axis == "Z" ? 1 : -1;
            return new Matrix(4, 4, kx, 0,  0,  0,
                                    0,  ky, 0,  0,
                                    0,  0,  kz, 0,
                                    0,  0,  0,  1);
        }

        private Matrix GetScaleMatrix(double c)
        {
            return new Matrix(4, 4, c, 0, 0, 0,
                                    0, c, 0, 0,
                                    0, 0, c, 0,
                                    0, 0, 0, 1);
        }

        public Point GetCenter()
        {
            double x = 0;
            double y = 0;
            double z = 0;
            int count = 0;

            Polygons.ForEach(p => p.Edges.ForEach(e =>
            {
                x += e.P1.X + e.P2.X;
                y += e.P1.Y + e.P2.Y;
                z += e.P1.Z + e.P2.Z;
                count += 2;
            }));

            return new Point(x/count, y/count, z/count);
        }
    }

    static class Shapes
    {
        static public Polyhedron GetCube(double x, double y, double z, double width)
        {
            return GetCube(new Point(x, y, z), width);
        }

        static public Polyhedron GetCube(Point start, double width)
        {
            double x = start.X;
            double y = start.Y;
            double z = start.Z;

            Point p1 = new Point(x,         y,         z);
            Point p2 = new Point(x + width, y,         z);
            Point p3 = new Point(x,         y + width, z);
            Point p4 = new Point(x + width, y + width, z);
            Point p5 = new Point(x,         y,         z + width);
            Point p6 = new Point(x + width, y,         z + width);
            Point p7 = new Point(x,         y + width, z + width);
            Point p8 = new Point(x + width, y + width, z + width);

            Edge e1 = new Edge(p1, p2);
            Edge e2 = new Edge(p1, p3);
            Edge e3 = new Edge(p2, p4);
            Edge e4 = new Edge(p3, p4);

            Edge e5 = new Edge(p5, p6);
            Edge e6 = new Edge(p5, p7);
            Edge e7 = new Edge(p6, p8);
            Edge e8 = new Edge(p7, p8);

            Edge e9 = new Edge(p1, p5);
            Edge e10 = new Edge(p2, p6);
            Edge e11 = new Edge(p3, p7);
            Edge e12 = new Edge(p4, p8);

            Polygon poly1 = new Polygon(new List<Edge>() { e1, e2, e3, e4 });
            Polygon poly2 = new Polygon(new List<Edge>() { e1, e9, e10, e5 });
            Polygon poly3 = new Polygon(new List<Edge>() { e2, e9, e11, e6 });
            Polygon poly4 = new Polygon(new List<Edge>() { e3, e10, e12, e7 });
            Polygon poly5 = new Polygon(new List<Edge>() { e4, e11, e12, e8 });
            Polygon poly6 = new Polygon(new List<Edge>() { e9, e10, e11, e12 });

            return new Polyhedron(new List<Polygon>() { poly1, poly2, poly3, poly4, poly5, poly6 });
        }

        static public Polyhedron GetTetrahedron(double x, double y, double z, double width)
        {
            return GetTetrahedron(new Point(x, y, z), width);
        }

        static public Polyhedron GetTetrahedron(Point start, double width)
        {
            double x = start.X;
            double y = start.Y;
            double z = start.Z;

            Point p1 = new Point(x, y, z);
            Point p2 = new Point(x + width, y + width, z);

            Point p3 = new Point(x + width, y, z + width);
            Point p4 = new Point(x, y + width, z + width);

            Edge e1 = new Edge(p1, p2);
            Edge e2 = new Edge(p3, p4);

            Edge e3 = new Edge(p1, p3);
            Edge e4 = new Edge(p1, p4);

            Edge e5 = new Edge(p2, p3);
            Edge e6 = new Edge(p2, p4);

            Polygon poly1 = new Polygon(new List<Edge>() { e2, e3, e4 });
            Polygon poly2 = new Polygon(new List<Edge>() { e2, e5, e6 });
            Polygon poly3 = new Polygon(new List<Edge>() { e1, e3, e5 });
            Polygon poly4 = new Polygon(new List<Edge>() { e1, e4, e6 });

            return new Polyhedron(new List<Polygon>() { poly1, poly2, poly3, poly4 });
        }

        static public Polyhedron GetOctahedron(double x, double y, double z, double width)
        {
            return GetOctahedron(new Point(x, y, z), width);
        }

        static public Polyhedron GetOctahedron(Point start, double width)
        {
            double x = start.X;
            double y = start.Y;
            double z = start.Z;

            double xCenter = x + width / 2;
            double yCenter = y + width / 2;
            double zCenter = z + width / 2;

            Point p1 = new Point(xCenter,   yCenter,   z);
            Point p2 = new Point(xCenter,   yCenter,   z + width);
            Point p3 = new Point(xCenter,   y,         zCenter);
            Point p4 = new Point(xCenter,   y + width, zCenter);
            Point p5 = new Point(x,         yCenter,   zCenter);
            Point p6 = new Point(x + width, yCenter,   zCenter);

            Edge e1 = new Edge(p1, p3);
            Edge e2 = new Edge(p1, p4);
            Edge e3 = new Edge(p1, p5);
            Edge e4 = new Edge(p1, p6);

            Edge e5 = new Edge(p2, p3);
            Edge e6 = new Edge(p2, p4);
            Edge e7 = new Edge(p2, p5);
            Edge e8 = new Edge(p2, p6);

            Edge e9 = new Edge(p3, p5);
            Edge e10 = new Edge(p3, p6);
            Edge e11 = new Edge(p4, p5);
            Edge e12 = new Edge(p4, p6);

            Polygon poly1 = new Polygon(new List<Edge>() { e1, e9, e3 });
            Polygon poly2 = new Polygon(new List<Edge>() { e1, e10, e4 });
            Polygon poly3 = new Polygon(new List<Edge>() { e2, e11, e3 });
            Polygon poly4 = new Polygon(new List<Edge>() { e2, e12, e4 });
            Polygon poly5 = new Polygon(new List<Edge>() { e5, e9, e7 });
            Polygon poly6 = new Polygon(new List<Edge>() { e5, e10, e8});
            Polygon poly7 = new Polygon(new List<Edge>() { e6, e11, e7 });
            Polygon poly8 = new Polygon(new List<Edge>() { e6, e12, e8 });

            return new Polyhedron(new List<Polygon>() { poly1, poly2, poly3, poly4, poly5, poly6, poly7, poly8 });
        }

        static public Polyhedron GetIcosahedron(Point start, double radius)
        {
            double phi = 1.6180;
            double coef = (double)Math.Sqrt(1 + phi * phi);

            List<Point> points = new List<Point>()
            {
                new Point(phi * radius / coef, 1 * radius / coef, 0),
                new Point(phi * radius / coef, -1 * radius / coef, 0),
                new Point(-phi * radius / coef, -1 * radius / coef, 0),
                new Point(-phi * radius / coef, 1 * radius / coef, 0),
                new Point(0, phi * radius / coef, 1 * radius / coef),
                new Point(0, -phi * radius / coef, 1 * radius / coef),
                new Point(0, -phi * radius / coef, -1 * radius / coef),
                new Point(0, phi * radius / coef, -1 * radius / coef),
                new Point(1 * radius / coef, 0, phi * radius / coef),
                new Point(1 * radius / coef, 0, -phi * radius / coef),
                new Point(-1 * radius / coef, 0, -phi * radius / coef),
                new Point(-1 * radius / coef, 0, phi * radius / coef),
            };

            List<Edge> edges = new List<Edge>
            {
                new Edge(points[7], points[0]),
                new Edge(points[7], points[4]),
                new Edge(points[7], points[3]),
                new Edge(points[7], points[10]),
                new Edge(points[7], points[9]),
                new Edge(points[5], points[8]),
                new Edge(points[5], points[11]),
                new Edge(points[5], points[2]),
                new Edge(points[5], points[6]),
                new Edge(points[5], points[1]),
                new Edge(points[0], points[8]),
                new Edge(points[0], points[4]),
                new Edge(points[8], points[4]),
                new Edge(points[8], points[11]),
                new Edge(points[4], points[11]),
                new Edge(points[4], points[3]),
                new Edge(points[11], points[3]),
                new Edge(points[11], points[2]),
                new Edge(points[3], points[2]),
                new Edge(points[3], points[10]),
                new Edge(points[2], points[10]),
                new Edge(points[2], points[6]),
                new Edge(points[10], points[6]),
                new Edge(points[10], points[9]),
                new Edge(points[6], points[9]),
                new Edge(points[6], points[1]),
                new Edge(points[9], points[1]),
                new Edge(points[9], points[0]),
                new Edge(points[1], points[0]),
                new Edge(points[1], points[8])
            };

            // TODO: Распределить Edge по разным полигонам, а не все в один (хотя мало понятно зачем)

            return new Polyhedron(new List<Polygon>() { new Polygon(edges) });
        }

        static public Polyhedron GetDodecahedron(Point start, double radius)
        {
            double phi = 1.6180;
            double coef = (double)Math.Sqrt(1 + phi * phi);

            List<Point> points = new List<Point>()
            {
                new Point(radius, radius, radius),
                new Point(radius, radius, -radius),
                new Point(radius, -radius, radius),
                new Point(radius, -radius, -radius),
                new Point(-radius, radius, radius),
                new Point(-radius, radius, -radius),
                new Point(-radius, -radius, radius),
                new Point(-radius, -radius, -radius),
                new Point(0, 1 * radius / phi, phi * radius),
                new Point(0, 1 * radius / phi, -phi * radius),
                new Point(0, -1 * radius / phi, phi * radius),
                new Point(0, -1 * radius / phi, -phi * radius),
                new Point(1 * radius / phi, phi * radius, 0),
                new Point(1 * radius / phi, -phi * radius, 0),
                new Point(-1 * radius / phi, phi * 35, 0),
                new Point(-1 * radius / phi, -phi * 35, 0),
                new Point(phi * radius, 0, 1 * radius / phi),
                new Point(phi * radius, 0, -1 * radius / phi),
                new Point(-phi * radius, 0, 1 * radius / phi),
                new Point(-phi * radius, 0, -1 * radius / phi),
            };

            List<Edge> edges = new List<Edge>
            {
                new Edge(points[8], points[10]),
                new Edge(points[8], points[0]),
                new Edge(points[8], points[4]),
                new Edge(points[10], points[2]),
                new Edge(points[10], points[6]),
                new Edge(points[9], points[11]),
                new Edge(points[9], points[1]),
                new Edge(points[9], points[5]),
                new Edge(points[11], points[3]),
                new Edge(points[11], points[7]),
                new Edge(points[12], points[14]),
                new Edge(points[13], points[15]),
                new Edge(points[12], points[0]),
                new Edge(points[12], points[1]),
                new Edge(points[14], points[4]),
                new Edge(points[14], points[5]),
                new Edge(points[13], points[2]),
                new Edge(points[13], points[3]),
                new Edge(points[15], points[6]),
                new Edge(points[15], points[7]),
                new Edge(points[16], points[17]),
                new Edge(points[18], points[19]),
                new Edge(points[16], points[0]),
                new Edge(points[16], points[2]),
                new Edge(points[17], points[1]),
                new Edge(points[17], points[3]),
                new Edge(points[18], points[4]),
                new Edge(points[18], points[6]),
                new Edge(points[19], points[5]),
                new Edge(points[19], points[7]),
            };


            // TODO: Распределить Edge по разным полигонам, а не все в один (хотя мало понятно зачем)

            return new Polyhedron(new List<Polygon>() { new Polygon(edges) });
        }
    }
}
