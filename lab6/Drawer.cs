﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace lab6
{
    class Drawer
    {
        RenderTargetBitmap bmp;
        int width; int height;

        Polyhedron polyhedron;
        Edge line = null;

        public Drawer(int width, int height)
        {
            this.width = width;
            this.height = height;
            bmp = new RenderTargetBitmap(width, height, 96, 96, PixelFormats.Pbgra32);
            SetCube();
        }

        public void SetLine(Edge e)
        {
            line = e;
            DrawShape();
        }

        public void SetCube()
        {
            polyhedron = Shapes.GetCube(new Point(-35, -35, -35), 70);
            DrawShape();
        }

        public void SetTetrahedron()
        {
            polyhedron = Shapes.GetTetrahedron(new Point(-35, -35, -35), 70);
            DrawShape();
        }

        public void SetOctahedron()
        {
            polyhedron = Shapes.GetOctahedron(new Point(-35, -35, -35), 80);
            DrawShape();
        }

        public void SetIcosahedron()
        {
            polyhedron = Shapes.GetIcosahedron(new Point(0, 0, 0), 50);
            DrawShape();
        }

        public void SetDodecahedron()
        {
            polyhedron = Shapes.GetDodecahedron(new Point(0, 0, 0), 35);
            DrawShape();
        }

        public void RotateShape(string axis, double angle)
        {
            polyhedron.Rotate(axis, angle);
            DrawShape();
        }

        public void RotateAroundLineShape(double angle)
        {
            polyhedron.RotateAroundLine(line, angle);
            DrawShape();
        }

        public void TranslateShape(double x, double y, double z)
        {
            polyhedron.Translate(x, y, z);
            DrawShape();
        }

        public void MirrorShape(string axis)
        {
            polyhedron.Mirror(axis);
            DrawShape();
        }

        public void ScaleShape(double c)
        {
            polyhedron.Scale(c);
            DrawShape();
        }

        public void DrawShape()
        {
            bmp.Clear();
            var edges = Camera.GetProjectionEdges(polyhedron);
            edges.ForEach(e => DrawLine(e));

            if (line != null)
            {
                Polyhedron linePol = new Polyhedron(new List<Polygon>() { new Polygon(new List<Edge>() { line }) });
                edges = Camera.GetProjectionEdges(linePol);
                edges.ForEach(e => DrawLine(e, 1, Brushes.Red));
            }
        }

        public void DrawLine(Edge e, int thickness = 1, SolidColorBrush brush = null)
        {
            DrawLine(e.P1, e.P2, thickness, brush);
        }

        public void DrawLine(Point p1, Point p2, int thickness = 1, SolidColorBrush brush = null)
        {
            DrawLine(p1.X, p1.Y, p2.X, p2.Y, thickness, brush);
        }

        public void DrawLine(double x1, double y1, double x2, double y2, int thickness = 1, SolidColorBrush brush = null)
        {
            DrawingVisual drawingVisual = new DrawingVisual();

            using (DrawingContext drawingContext = drawingVisual.RenderOpen())
            {
                Pen pen = new Pen(brush == null ? Brushes.Black : brush, thickness);
                System.Windows.Point startPoint = new System.Windows.Point(x1 + width / 2, y1 + height / 2);
                System.Windows.Point endPoint = new System.Windows.Point(x2 + width / 2, y2 + height / 2);

                drawingContext.DrawLine(pen, startPoint, endPoint);
            }

            bmp.Render(drawingVisual);
        }

        public RenderTargetBitmap GetBitmap()
        {
            return bmp;
        }
    }
}
