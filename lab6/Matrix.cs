﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6
{
    class Matrix
    {
        public double[,] Matr { get; set; }
        public int N { get; set; }
        public int M { get; set; }

        public Matrix(int n, int m)
        {
            Init(n, m);
        }

        public Matrix(int n, int m, params double[] values)
        {
            Init(n, m);

            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                {
                    Matr[i, j] = values[i * n + j];
                }

        }

        public Matrix(int n, int m, double[,] matr)
        {
            Matr = matr;
            N = n;
            M = m;
        }

        public double this[int i, int j]
        {
            get { return Matr[i, j]; }
            set { Matr[i, j] = value; }
        }

        public Matrix GetMultBy(Matrix m)
        {
            if (M != m.N)
            {
                throw new InvalidOperationException("Умножение матриц невозможно: количество столбцов первой матрицы не равно количеству строк второй матрицы.");
            }

            Matrix result = new Matrix(N, m.M);

            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < m.M; j++)
                {
                    double sum = 0;

                    for (int k = 0; k < M; k++)
                    {
                        sum += Matr[i, k] * m.Matr[k, j];
                    }

                    result.Matr[i, j] = sum;
                }
            }

            return result;
        }


        private void Init(int n, int m)
        {
            Matr = new double[n, m];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                {
                    Matr[i, j] = 0;
                }
            N = n;
            M = m;
        }

        static public Matrix FromPoint(Point p, double shift = 0)
        {
            return new Matrix(1, 4, p.X + shift, p.Y + shift, p.Z + shift, 1);
        }
    }
}
