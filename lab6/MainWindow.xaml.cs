﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace lab6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Drawer drawer;

        public MainWindow()
        {
            InitializeComponent();

            drawer = new Drawer(600, 600);
            
            Projection.SelectedIndex = 0;
        }

        public void UpdateImage()
        {
            Image.Source = drawer.GetBitmap();
        }

        private void Projection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedComboBoxItem = (TextBlock)(sender as ComboBox).SelectedItem;
            switch (selectedComboBoxItem.Text)
            {
                case "Параллельная":
                    Camera.currentProjection = Camera.Projections.PARALLEL; break;
                case "Перспективная":
                    Camera.currentProjection = Camera.Projections.PERSPECTIVE; break;
                case "Аксонометрическая":
                    Camera.currentProjection = Camera.Projections.AXONOMETRIC; break;
            }
            drawer.DrawShape();
            UpdateImage();
        }

        private void ButtonCube_Click(object sender, RoutedEventArgs e)
        {
            drawer.SetCube();
            UpdateImage();
        }

        private void ButtonTetrahedron_Click(object sender, RoutedEventArgs e)
        {
            drawer.SetTetrahedron();
            UpdateImage();
        }

        private void ButtonOctahedron_Click(object sender, RoutedEventArgs e)
        {
            drawer.SetOctahedron();
            UpdateImage();
        }

        private void ButtonIcosahedron_Click(object sender, RoutedEventArgs e)
        {
            drawer.SetIcosahedron();
            UpdateImage();
        }

        private void ButtonDodecahedron_Click(object sender, RoutedEventArgs e)
        {
            drawer.SetDodecahedron();
            UpdateImage();
        }

        private void ButtonRotate_Click(object sender, RoutedEventArgs e)
        {
            drawer.RotateShape(RotateAxis.Text, double.Parse(RotateAngle.Text));
            UpdateImage();
        }

        private void ButtonTranslate_Click(object sender, RoutedEventArgs e)
        {
            drawer.TranslateShape(double.Parse(TranslateX.Text), double.Parse(TranslateY.Text), double.Parse(TranslateZ.Text));
            UpdateImage();
        }

        private void ButtonMirror_Click(object sender, RoutedEventArgs e)
        {
            drawer.MirrorShape(MirrorAxis.Text);
            UpdateImage();
        }

        private void ButtonScale_Click(object sender, RoutedEventArgs e)
        {
            drawer.ScaleShape(double.Parse(ScaleCoeff.Text));
            UpdateImage();
        }

        private void ButtonRotateLine_Click(object sender, RoutedEventArgs e)
        {
            drawer.RotateAroundLineShape(double.Parse(RotateLineAngle.Text));
            UpdateImage();
        }

        private void ButtonDefineLine_Click(object sender, RoutedEventArgs e)
        {
            double x1 = double.Parse(Point1X.Text);
            double x2 = double.Parse(Point2X.Text);
            double y1 = double.Parse(Point1Y.Text);
            double y2 = double.Parse(Point2Y.Text);
            double z1 = double.Parse(Point1Z.Text);
            double z2 = double.Parse(Point2Z.Text);
            Point p1 = new Point(GetScaledCoords(x1, x2, 15), GetScaledCoords(y1, y2, 15), GetScaledCoords(z1, z2, 15));
            Point p2 = new Point(GetScaledCoords(x1, x2, -15), GetScaledCoords(y1, y2, -15), GetScaledCoords(z1, z2, -15));

            if (Point.Distance(p1, p2) > 0)
            {
                ClearLineButton.IsEnabled = true;
                RotateLineButton.IsEnabled = true;
                drawer.SetLine(new Edge(p1, p2));
                UpdateImage();
            }
        }

        private void ButtonClearLine_Click(object sender, RoutedEventArgs e)
        {
            ClearLineButton.IsEnabled = false;
            RotateLineButton.IsEnabled = false;
            drawer.SetLine(null);
            UpdateImage();
        }

        private double GetScaledCoords(double a1, double a2, double k)
        {
            return a1 + Math.Abs(a2 - a1) * k;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
          
        }
    }
}
