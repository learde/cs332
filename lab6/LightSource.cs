﻿using System;
using System.Collections.Generic;
using System.Drawing;
using lab6;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6
{
    internal class LightSource
    {
        public Vector Coordinate { get; set; }
        public Color Color { get; set; }

        public LightSource(Vector coordinate, Color color)
        {
            Coordinate = coordinate;
            Color = color;
        }
    }
}
