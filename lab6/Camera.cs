﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6
{
    static class Camera
    {
        public enum Projections { PERSPECTIVE, PARALLEL, AXONOMETRIC };

        static public Projections currentProjection = Projections.PARALLEL;

        static public List<Edge> GetProjectionEdges(Polyhedron polyhedron)
        {
            switch (currentProjection)
            {
                case Projections.PARALLEL:
                    return GetParallelProjectionEdges(polyhedron);
                case Projections.PERSPECTIVE:
                    return GetPerspectiveProjectionEdges(polyhedron);
                case Projections.AXONOMETRIC:
                    return GetAxonometricProjectionEdges(polyhedron);
                default:
                    return new List<Edge>();
            }
        }

        static public Matrix PerspectiveProjectionMatrix = new Matrix(4, 4, 
                                                               1, 0, 0, 0,
                                                               0, 1, 0, 0,
                                                               0, 0, 0, -1 / -160.0,
                                                               0, 0, 0, 1);

        static public double Psi = 40;
        static public double Phi = 40;
        static public Matrix AxonometricProjectionMatrix = new Matrix(4, 4,
                                                               Utils.Cos(Psi), Utils.Sin(Psi) * Utils.Sin(Phi),  0, 0,
                                                               0,              Utils.Cos(Phi),                   0, 0,
                                                               Utils.Sin(Psi), -Utils.Sin(Phi) * Utils.Cos(Psi), 0, 0,
                                                               0,              0,                                0, 1);
        
        static public List<Edge> GetPerspectiveProjectionEdges(Polyhedron polyhedron)
        {
            List<Edge> allEdges = GetAllEdges(polyhedron);

            allEdges = allEdges.Select(e =>
            {
                Matrix m1 = Matrix.FromPoint(e.P1);
                Matrix m2 = Matrix.FromPoint(e.P2);

                Matrix res1 = m1.GetMultBy(PerspectiveProjectionMatrix);
                Matrix res2 = m2.GetMultBy(PerspectiveProjectionMatrix);

                return new Edge(new Point(res1[0, 0] / res1[0, 3], res1[0, 1] / res1[0, 3], res1[0, 2]), 
                                new Point(res2[0, 0] / res2[0, 3], res2[0, 1] / res2[0, 3], res2[0, 2]));
            }).ToList();

            return GetNormalizedEdges(allEdges);
        }

        static public List<Edge> GetAxonometricProjectionEdges(Polyhedron polyhedron)
        {
            List<Edge> allEdges = GetAllEdges(polyhedron);

            allEdges = allEdges.Select(e =>
            {
                Matrix m1 = Matrix.FromPoint(e.P1);
                Matrix m2 = Matrix.FromPoint(e.P2);

                Matrix res1 = m1.GetMultBy(AxonometricProjectionMatrix);
                Matrix res2 = m2.GetMultBy(AxonometricProjectionMatrix);

                return new Edge(new Point(res1[0, 0], res1[0, 1], res1[0, 2]),
                                new Point(res2[0, 0], res2[0, 1], res2[0, 2]));
            }).ToList();

            return GetNormalizedEdges(allEdges);
        }

        static public List<Edge> GetParallelProjectionEdges(Polyhedron polyhedron)
        {
            List<Edge> allEdges = GetAllEdges(polyhedron);

            return GetNormalizedEdges(allEdges);
        }

        static private List<Edge> GetNormalizedEdges(List<Edge> edges)
        {
            edges = NullZCoordinate(edges);

            edges = ClearZeroLength(edges);

            return new HashSet<Edge>(edges).ToList();
        }

        static private List<Edge> GetAllEdges(Polyhedron polyhedron)
        {
            List<Edge> allEdges = new List<Edge>();

            polyhedron.Polygons.ForEach(p => p.Edges.ForEach(e => allEdges.Add(new Edge(e))));

            return allEdges;
        }

        static private List<Edge> NullZCoordinate(List<Edge> edges)
        {
            return edges.Select(e =>
            {
                e.P1.Z = 0;
                e.P2.Z = 0;
                return e;
            }).ToList();
        }

        static private List<Edge> ClearZeroLength(List<Edge> edges)
        {
            return edges.Where(e => e.GetLength() != 0).ToList();
        }
    }
}
