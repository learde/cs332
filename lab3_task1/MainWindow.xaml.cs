﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Drawing.Imaging;
using static System.Net.Mime.MediaTypeNames;

namespace lab4
{

    public partial class MainWindow : Window
    {
        enum Type
        {
            Pen,
            Fill,
            FillImage,
            Hightlight,
        }

        private bool _isDrawing = false;
        private System.Windows.Point _point;

        // general
        Drawer.Drawer _drawer;
        readonly List<Button> _buttons = new List<Button>();
        Drawer.Color _color;
        Type _type;
        private Bitmap _bitmap;
        private BitmapImage _image;

        public MainWindow()
        {
            InitializeComponent();
            _type = Type.Pen;
            _color = new Drawer.Color(0, 0, 0);
            _drawer = new Drawer.Drawer(800, 650);
            UpdateImage();
            ContentRendered += OnContentRendered;
            UnhighlightInterface();
        }

        private void OnContentRendered(object? sender, EventArgs e) => FindButtons(this);

        private void FindButtons(DependencyObject root)
        {
            Stack<DependencyObject> objects = new Stack<DependencyObject>();
            
            objects.Push(root);
            
            while (objects.Count > 0)
            {
                var x = objects.Pop();
                if (x is Button button)
                {
                    _buttons.Add(button);
                }

                int count = VisualTreeHelper.GetChildrenCount(x);
                for (int i = 0; i < count; i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(x, i);
                    objects.Push(child);
                }
            }
        }

        void UnhighlightInterface()
        {
            _buttons.ForEach(button =>
            {
                button.IsEnabled = true;
                button.BorderBrush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(10, 10, 10));
            });
            if (_bitmap is null)
            {
                FillImageButton.IsEnabled = false;
            }
        }

        void HightlightButton(Button button)
        {
            button.IsEnabled = true;
            button.BorderBrush = System.Windows.Media.Brushes.Red;
        }

        void UpdateImage()
        {
            InitialImage.Source = _drawer.GetBitmap();
        }

        private void ColorPicker_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<System.Windows.Media.Color?> e)
        {
            if (e.NewValue.HasValue)
            {
                _color = new Drawer.Color(e.NewValue.Value);
            }
        }
        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _point = e.GetPosition(InitialImage);
            var x = (int)_point.X;
            var y = (int)_point.Y;
            _drawer.SetBaseColor(x, y);
            switch (_type)
            {
                case Type.Fill:
                    _drawer.Fill(x, y, _color);
                    break;
                case Type.Pen:
                    _isDrawing = true;
                    break;
                case Type.FillImage:
                    _drawer.FillImage(x, y, _bitmap, new Drawer.Point(x,y));
                    break;
                case Type.Hightlight:
                    _drawer.Highlight(x, y, _color);
                    break;
            }

        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            _drawer.FillWhite();
            UpdateImage();
        }

        private void Image_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _isDrawing = false;
        }

        private void Image_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_isDrawing)
                return;

            var pos = e.GetPosition(InitialImage);
            var posX = (int)pos.X;
            var posY = (int)pos.Y;

            _drawer.DrawLineBresenham((int)_point.X, (int)_point.Y, posX, posY, new byte[] { 0, 0, 0, 255 });
            _point = new System.Windows.Point(posX, posY);
            UpdateImage();
        }


        private void FillButton_Click(object sender, RoutedEventArgs e)
        {
            UnhighlightInterface();
            _type = Type.Fill;
            InitialImage.Cursor = Drawer.Cursors.Fill;
            HightlightButton(FillButton);
        }


        private void PenButton_Click(Object sender, RoutedEventArgs e)
        {
            UnhighlightInterface();
            _type = Type.Pen;
            InitialImage.Cursor = Drawer.Cursors.Pen;
            HightlightButton(PenButton);
        }

        private void FillImageButton_Click(Object sender, RoutedEventArgs e)
        {
            UnhighlightInterface();
            _type = Type.FillImage;
            InitialImage.Cursor = Drawer.Cursors.Fill;
            HightlightButton(FillImageButton);
        }

        private void HightLightButton_Click(Object sender, RoutedEventArgs e)
        {
            UnhighlightInterface();
            _type = Type.Hightlight;
            InitialImage.Cursor = Cursors.Hand;
            HightlightButton(HightLightButton);
        }


        private void LoadButton_Click(object sender, EventArgs e)
        {
            var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "Изображения|*.jpeg;*.jpg";

            if (openFileDialog.ShowDialog() == true)
            {
                var path = openFileDialog.FileName;
                _image = new BitmapImage(new Uri(path));
                LoadImage.Source = _image;
                _bitmap = new Bitmap(path);
                FillImageButton.IsEnabled = true;
            }
        }

    }
}
