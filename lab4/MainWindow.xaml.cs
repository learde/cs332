﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Drawer;

namespace lab4
{

    public partial class MainWindow : Window
    {
        private enum Primitive
        {
            NONE,
            POINT,
            LINE,
            POLYGON,
        }

        Drawer.Drawer _drawer;
        private readonly List<Line> _lines = new();
        private readonly List<Polygon> _polygons = new();
        private readonly List<Drawer.Point> _points = new();
        Primitive _drawingPrimitive = Primitive.NONE;


        private readonly List<Button> _buttons = new();
        private readonly List<ComboBox> _selects = new();
        private readonly List<TextBox> _textBoxes = new();
        byte[] _color = Drawer.Colors.Black.ToBgra();
        private Action<Line> _lineDrawn;
        private Action<Drawer.Point> _pointDrawn;

        // editing
        bool isEditing = false;


        Drawer.Line drawingLine = null;
        Drawer.Polygon drawingPolygon = null;
        WriteableBitmap bitmapBefore = null;

        public MainWindow()
        {
            InitializeComponent();
            _drawer = new Drawer.Drawer(800, 650);
            UpdateImage();
            ContentRendered += OnContentRendered;
        }


        private void OnContentRendered(object? sender, EventArgs e)
        {
            Utilities.Find(this, _buttons);
            Utilities.Find(this, _selects);
            Utilities.Find(this, _textBoxes);

            _selects.ForEach(x =>
            {
                x.DisplayMemberPath = "Title";
                x.SelectedValuePath = "Id";
                x.ItemsSource = null;
            });
        }

        void DisableInterface()
        {
            _buttons.ForEach(button => button.IsEnabled = false);
            _selects.ForEach(select => select.IsEnabled = false);
            _textBoxes.ForEach(textBox => textBox.IsEnabled = false);
        }

        void EnableInterface()
        {
            _buttons.ForEach(button =>
            {
                button.IsEnabled = true;
                button.BorderBrush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(112, 112, 112));
            });

            _selects.ForEach(select => select.IsEnabled = true);
            _textBoxes.ForEach(textBox => textBox.IsEnabled = true);
        }

        void HightlightButton(Button btn)
        {
            btn.IsEnabled = true;
            btn.BorderBrush = Brushes.Red;
        }

        void StartImageEditing(Primitive type)
        {
            isEditing = true;
            //image.Cursor = Cursors.Pen;
            _drawingPrimitive = type;
        }

        void StopImageEditing()
        {
            isEditing = false;
            //image.Cursor = Cursors.Arrow;
            _drawingPrimitive = Primitive.NONE;
        }

        void UpdateImage()
        {
            image.Source = _drawer.Bitmap;
        }

        private void definePoint_Click(object sender, RoutedEventArgs e)
        {
            DisableInterface();
            HightlightButton(definePoint);
            StartImageEditing(Primitive.POINT);
        }

        private void defineLine_Click(object sender, RoutedEventArgs e)
        {
            DisableInterface();
            HightlightButton(defineLine);
            StartImageEditing(Primitive.LINE);
        }

        private void definePolygon_Click(object sender, RoutedEventArgs e)
        {
            DisableInterface();
            HightlightButton(definePolygon);
            StartImageEditing(Primitive.POLYGON);
        }

        private void DrawLineFirst(int x, int y)
        {
            bitmapBefore = new WriteableBitmap(_drawer.Bitmap);
            drawingLine = new Line(x, y);
        }

        private void DrawLineSecond(int x, int y)
        {
            drawingLine.Last = new(x, y);
            drawingLine.Id = _lines.Count();
            _lines.Add(drawingLine);
            _drawer.DrawLineBresenham(drawingLine, Drawer.Colors.Black);
            _lineDrawn?.Invoke(drawingLine);

            LineSelect.ItemsSource = null;
            LineSelect.ItemsSource = _lines;

            drawingLine = null;
            bitmapBefore = null;
            EnableInterface();
            StopImageEditing();
        }

        private Drawer.Color _c = Drawer.Colors.Red;

        private void DrawPoint(int x, int y)
        {
            var point = new Drawer.Point(x, y);
            _pointDrawn?.Invoke(point);
            _drawer.DrawPoint(point, _c, 6);
            _c = Drawer.Colors.Red;
            _points.Add(point);
            EnableInterface();
            StopImageEditing();
        }

        private void DrawPolygonFirst(int x, int y)
        {
            bitmapBefore = new WriteableBitmap(_drawer.Bitmap);
            drawingPolygon = new Polygon(x, y);
        }

        private void DrawPolygonAnother(int x, int y)
        {
            Drawer.Point lastPoint = drawingPolygon.Points.Last();
            Drawer.Point firstPoint = drawingPolygon.Points.First();

            Drawer.Point currentPoint = new Drawer.Point(x, y);
            int dist = Drawer.Point.Distance(firstPoint, currentPoint);

            if (dist > 30)
            {
                _drawer.DrawLineBresenham(lastPoint.X, lastPoint.Y, x, y, _color);
                drawingPolygon.Add(x, y);
                bitmapBefore = new WriteableBitmap(_drawer.Bitmap);
            }
            else
            {
                _drawer.DrawLineBresenham(lastPoint.X, lastPoint.Y, firstPoint.X, firstPoint.Y, _color);

                drawingPolygon.Id = _polygons.Count();
                _polygons.Add(drawingPolygon);

                PolygonSelect.ItemsSource = null;
                PolygonSelect.ItemsSource = _polygons;

                bitmapBefore = null;
                drawingPolygon = null;

                EnableInterface();
                StopImageEditing();
            }
        }


        private void Redraw()
        {
            _drawer.Clear();
            _polygons.ForEach(x => _drawer.DrawPolygon(x, Drawer.Colors.Black));
            _lines.ForEach(x => _drawer.DrawLineBresenham(x, Drawer.Colors.Black));
            _points.ForEach(x => _drawer.DrawPoint(x, Drawer.Colors.Red, 6));
        }

        private void image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!isEditing)
                return;

            var pos = e.GetPosition(image);
            var posX = (int)pos.X;
            var posY = (int)pos.Y;

            switch (_drawingPrimitive)
            {
                case Primitive.POINT:
                    DrawPoint(posX, posY);
                    break;
                case Primitive.LINE:
                    if (drawingLine == null)
                    {
                        DrawLineFirst(posX, posY);
                    }
                    else
                    {
                        DrawLineSecond(posX, posY);
                    }
                    break;
                case Primitive.POLYGON:
                    if (drawingPolygon == null)
                    {
                        DrawPolygonFirst(posX, posY);
                    }
                    else
                    {
                        DrawPolygonAnother(posX, posY);
                    }
                    break;
            }
            UpdateImage();
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            _lines.Clear();
            _polygons.Clear();
            _points.Clear();
            _selects.ForEach(x => x.ItemsSource = null);
            _drawer.Clear();
            UpdateImage();
        }

        private void image_MouseMove(object sender, MouseEventArgs e)
        {
            if (!isEditing)
                return;

            if (_drawingPrimitive == Primitive.LINE && drawingLine != null && bitmapBefore != null)
            {
                var pos = e.GetPosition(image);
                var posX = (int)pos.X;
                var posY = (int)pos.Y;
                _drawer.Bitmap = bitmapBefore;
                _drawer.DrawLineBresenham(drawingLine.X1, drawingLine.Y1, posX, posY, _color);
                UpdateImage();
            }

            if (_drawingPrimitive == Primitive.POLYGON && drawingPolygon != null && bitmapBefore != null)
            {
                var pos = e.GetPosition(image);
                var posX = (int)pos.X;
                var posY = (int)pos.Y;
                _drawer.Bitmap = bitmapBefore;

                Drawer.Point firstPoint = drawingPolygon.Points.First();
                Drawer.Point lastPoint = drawingPolygon.Points.Last();
                Drawer.Point currentPoint = new Drawer.Point(posX, posY);
                int dist = Drawer.Point.Distance(firstPoint, currentPoint);

                if (dist > 30)
                {
                    _drawer.DrawLineBresenham(lastPoint.X, lastPoint.Y, posX, posY, _color);
                }
                else
                {
                    _drawer.DrawLineBresenham(lastPoint.X, lastPoint.Y, firstPoint.X, firstPoint.Y, _color);
                }
                UpdateImage();
            }
        }

        private void Intersection_Click(object sender, RoutedEventArgs e)
        {
            if (LineSelect.SelectedItem is null)
                return;
            defineLine_Click(sender, e);
            _lineDrawn += OnLineDrawn;
        }

        private void OnLineDrawn(Line line)
        {
            _lineDrawn -= OnLineDrawn;
            var line1 = (Line)LineSelect.SelectedItem;

            var point = line.Intersection(line1);

            if (point is null)
                return;

            _points.Add(point);
            _drawer.DrawPoint(point, Drawer.Colors.Red, 6);
            UpdateImage();
        }


        private void ScaleCenter_Click(object sender, RoutedEventArgs e)
        {
            var polygon = (Polygon)PolygonSelect.SelectedItem;
            if (polygon is null)
                return;
            Scale(polygon.Center);
        }


        private void ScalePoint_Click(object sender, RoutedEventArgs e)
        {
            if (PolygonSelect.SelectedItem is null)
                return;
            definePoint_Click(sender, e);
            _pointDrawn += Scale;
        }


        private void Scale(Drawer.Point point)
        {
            _pointDrawn -= Scale;
            var polygon = (Polygon)PolygonSelect.SelectedItem;
            if (!float.TryParse(ScaleX.Text, out float dx) || !float.TryParse(ScaleY.Text, out float dy) || polygon is null)
                return;

            AffineTransformator.Transform(
                Transformations.Translation(-point.X, -point.Y) *
                Transformations.Scale(dx, dy) *
                Transformations.Translation(point.X, point.Y),
                polygon.Points
            );
            Redraw();
        }


        private void Translation_Click(object sender, RoutedEventArgs e)
        {
            var polygon = (Polygon)PolygonSelect.SelectedItem;
            if (!int.TryParse(Dx.Text, out int dx) || !int.TryParse(Dy.Text, out int dy) || polygon is null)
                return;

            AffineTransformator.Transform(
                Transformations.Translation(dx, dy),
                polygon.Points
            );
            Redraw();
        }



        private void RotateCenter_Click(object sender, RoutedEventArgs e)
        {
            var polygon = (Polygon)PolygonSelect.SelectedItem;
            if (polygon is null)
                return;
            RotatePolygon(polygon.Center);
        }



        private void RotatePoint_Click(object sender, RoutedEventArgs e)
        {
            if (PolygonSelect.SelectedItem is null)
                return;
            definePoint_Click(sender, e);
            _pointDrawn += RotatePolygon;
        }

        private void RotatePolygon(Drawer.Point point)
        {
            _pointDrawn -= RotatePolygon;
            var polygon = (Polygon)PolygonSelect.SelectedItem;
            if (!int.TryParse(Angle.Text, out int angle))
                return;

            AffineTransformator.Transform(
                Transformations.Translation(-point.X, -point.Y) *
                Transformations.Rotate(angle) *
                Transformations.Translation(point.X, point.Y),
                polygon.Points
            );
            Redraw();
        }

        private void LineRotate_Click(object sender, RoutedEventArgs e)
        {
            var line = (Line)LineSelect.SelectedItem;
            if (line is null)
                return;

            var center = line.Center;

            AffineTransformator.Transform(
                Transformations.Translation(-center.X, -center.Y) *
                Transformations.Rotate(90) *
                Transformations.Translation(center.X, center.Y),
                line.Points
            );
            Redraw();
        }

        private void PolygonContains_Click(object sender, RoutedEventArgs e)
        {
            var polygon = (Polygon)PolygonSelect.SelectedItem;
            if (polygon is null)
                return;
            _pointDrawn += Contains;
            definePoint_Click(sender, e);
        }

        private void Contains(Drawer.Point point)
        {
            _pointDrawn -= Contains;
            var polygon = (Polygon)PolygonSelect.SelectedItem;
            if (polygon.Contains(point))
                _c = Drawer.Colors.Green;
        }

        private void PointClassification_Click(object sender, RoutedEventArgs e)
        {
            var line = (Line)LineSelect.SelectedItem;
            if (line is null)
                return;
            _pointDrawn += Classification;
            definePoint_Click(sender, e);
        }


        private void Classification(Drawer.Point point)
        {
            var line = (Line)LineSelect.SelectedItem;
            if (line.IsRight(point))
                _c = Drawer.Colors.Green;
            else
                _c = Drawer.Colors.Blue;
        }
    }
}
