﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Drawing;
using System.Drawing.Imaging;
using FastBitmap;
using System.Windows.Interop;

namespace lab2
{
    public partial class MainWindow : Window
    {

        public enum Channel
        {
            R = 2,
            G = 1,
            B = 0,
        }

        public MainWindow()
        {
            InitializeComponent();
        }

        private void FillImages(string path)
        {
            InitialImage.Source = new BitmapImage(new Uri(path));
            RedImage.Source = IsolateChannel(path, Channel.R, HistogramRed);
            GreenImage.Source = IsolateChannel(path, Channel.G, HistogramGreen);
            BlueImage.Source = IsolateChannel(path, Channel.B, HistogramBlue);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "Изображения|*.jpeg;*.jpg";

            if (openFileDialog.ShowDialog() == true)
            {
                FillImages(openFileDialog.FileName);
            }
        }

        private BitmapSource BitmapToBitmapSource(Bitmap bmp)
        {
            return Imaging.CreateBitmapSourceFromHBitmap(
                bmp.GetHbitmap(),
                IntPtr.Zero,
                Int32Rect.Empty,
                BitmapSizeOptions.FromEmptyOptions()
            );
        }

        public BitmapSource IsolateChannel(string path, Channel channel, Canvas histogram)
        {
            var bitmap = new Bitmap(path);
            using (var fastBitmap = new FastBitmap.FastBitmap(bitmap))
            {
                for (var x = 0; x < fastBitmap.Width; x++)
                    for (var y = 0; y < fastBitmap.Height; y++)
                    {
                        var color = fastBitmap[x, y];
                        switch (channel)
                        {
                            case Channel.R:
                                fastBitmap[x, y] = Color.FromArgb(color.R, 0, 0);
                                break;
                            case Channel.G:
                                fastBitmap[x, y] = Color.FromArgb(0, color.G, 0);
                                break;
                            case Channel.B:
                                fastBitmap[x, y] = Color.FromArgb(0, 0, color.B);
                                break;
                        }
                    }
            }
            FillHistogram(histogram, bitmap, channel);
            return BitmapToBitmapSource(bitmap);
        }


        private void FillHistogram(Canvas canvas, Bitmap bitmap, Channel channel)
        {
            canvas.Children.Clear();
            var widthRect = canvas.ActualWidth / 256;
            var intensities = new int[256];

            Rectangle rectangle = new(0, 0, bitmap.Width, bitmap.Height);
            BitmapData bitmapData = bitmap.LockBits(
                rectangle,
                ImageLockMode.ReadWrite,
                bitmap.PixelFormat
            );

            IntPtr ptr = bitmapData.Scan0;

            int bytes = Math.Abs(bitmapData.Stride) * bitmap.Height;
            byte[] rgbValues = new byte[bytes];

            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            for (int counter = 0; counter + 2 < rgbValues.Length; counter += 3)
            {
                intensities[rgbValues[counter+(int)channel]]++;
            }

            bitmap.UnlockBits(bitmapData);

            var maxIntensity = intensities.Max();
            var coeff = canvas.ActualHeight / maxIntensity;
            var correctedIntensities = intensities.Select(i => (i * coeff)).ToArray();

            for (int i = 0; i < correctedIntensities.Length; i++)
            {
                var rect = new System.Windows.Shapes.Rectangle
                {
                    Width = widthRect,
                    Height = correctedIntensities[i],
                    Fill = System.Windows.Media.Brushes.BlueViolet
                };
                Canvas.SetTop(rect, canvas.ActualHeight - rect.Height);
                Canvas.SetLeft(rect, i * widthRect);
                canvas.Children.Add(rect);
            }

        }

    }
}
