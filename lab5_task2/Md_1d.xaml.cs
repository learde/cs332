﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Drawer;

namespace lab5_task2
{
    /// <summary>
    /// Логика взаимодействия для Md_1d.xaml
    /// </summary>
    public partial class Md_1d : Window
    {
        double roughness = 0.2;
        int heightLeft = 350;
        int heightRight = 150;
        int lengthLimit = 10;
        int delay = 0;

        int windowWidth = 600;
        int windowHeight = 450;

        Drawer.Drawer drawer;
        LinkedList<Drawer.Point> points = new LinkedList<Drawer.Point>();
        public Md_1d()
        {
            InitializeComponent();

            Roughness.Text = roughness.ToString();
            HeightLeft.Text = heightLeft.ToString();
            HeightRight.Text = heightRight.ToString();
            LimitLength.Text = lengthLimit.ToString();
            Delay.Text = delay.ToString();

            drawer = new Drawer.Drawer(windowWidth, windowHeight);
            UpdateImage();

            ApplyAlgorythm();
            DrawLine();
        }

        private async void ApplyAlgorythm()
        {
            points.Clear();

            points.AddFirst(new Drawer.Point(20, heightLeft));
            points.AddLast(new Drawer.Point(580, heightRight));

            var point = points.First;
            while (point.Next != null)
            {
                var nextPoint = point.Next;
                var length = Drawer.Point.Distance(point.Value, nextPoint.Value);
                if (length > lengthLimit)
                {
                    int newX = (point.Value.X + nextPoint.Value.X) / 2;
                    int newY = (point.Value.Y + nextPoint.Value.Y) / 2 + (int)GetRandomNumber(-roughness * length, roughness * length);
                    var newPoint = new Drawer.Point(newX, newY);
                    points.AddAfter(point, newPoint);
                }
                else
                {
                    point = nextPoint;
                }

                DrawLine();
                await Task.Delay(delay);
            }
        }

        private void DrawLine()
        {
            drawer.Clear();
            var point = points.First;
            while (point.Next != null)
            {
                var y = point.Value.Y;
                if (y <= 5) y = 5;
                if (y >= windowHeight-5) y = windowHeight - 5;
                var y2 = point.Next.Value.Y;
                if (y2 <= 5) y2 = 5;
                if (y2 >= windowHeight-5) y2 = windowHeight - 5;
                drawer.DrawLineBresenham(point.Value.X, y, point.Next.Value.X, y2, new byte[4] { 0, 0, 0, 255 });
                point = point.Next;
            }
            UpdateImage();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9,]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void UpdateImage()
        {
            Image.Source = drawer.Bitmap;
        }

        private double GetRandomNumber(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        private void UpdateParams()
        {
            int.TryParse(HeightLeft.Text, out heightLeft);
            int.TryParse(HeightRight.Text, out heightRight);
            double.TryParse(Roughness.Text, out roughness);
            int.TryParse(LimitLength.Text, out lengthLimit);
            int.TryParse(Delay.Text, out delay);

            ApplyAlgorythm();
            DrawLine();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            drawer.Clear();
            UpdateParams();
        }
    }
}
