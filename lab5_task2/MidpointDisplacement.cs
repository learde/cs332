﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Threading;
using Drawer;

namespace MD
{
    public static class ColorConverter
    {
        public static (double r, double g, double b) HslToRgb(double h, double s, double l)
        {
            h = Clamp(h, 0, 1);
            s = Clamp(s, 0, 1);
            l = Clamp(l, 0, 1);

            double r, g, b;

            if (s == 0)
            {
                r = g = b = l;
            }
            else
            {
                double HueToRgb(double p, double q, double t)
                {
                    if (t < 0) t += 1;
                    if (t > 1) t -= 1;
                    if (t < 1.0 / 6) return p + (q - p) * 6 * t;
                    if (t < 1.0 / 2) return q;
                    if (t < 2.0 / 3) return p + (q - p) * (2.0 / 3 - t) * 6;
                    return p;
                }

                double q = l < 0.5 ? l * (1 + s) : l + s - l * s;
                double p = 2 * l - q;

                r = HueToRgb(p, q, h + 1.0 / 3);
                g = HueToRgb(p, q, h);
                b = HueToRgb(p, q, h - 1.0 / 3);
            }

            return (r, g, b);
        }

        private static double Clamp(double value, double min, double max)
        {
            return Math.Max(min, Math.Min(max, value));
        }
    }


    public class MidpointDisplacement
    {
        Drawer.Drawer drawer;
        int[,] landscape;
        int width;
        int height;
        Func<int, Drawer.Color> getHeightColor;
        double roughness;

        bool isAnimated = false;

        public WriteableBitmap Bitmap
        {
            get => drawer.Bitmap;
        }

        public bool IsAnimated
        {
            get => isAnimated;
            set => isAnimated = value;
        }

        public MidpointDisplacement(int width, int height)
        {
            landscape = new int[width, height];
            this.width = width;
            this.height = height;

            drawer = new Drawer.Drawer(width, height);
            getHeightColor = GetHeightColor_BlackWhite;
        }

        public void SetCornersHeight(int topLeft, int topRight, int bottomLeft, int bottomRight)
        {
            landscape[0, 0] = topLeft;
            landscape[width - 1, 0] = topRight;
            landscape[0, height - 1] = bottomLeft;
            landscape[width - 1, height - 1] = bottomRight;
        }

        public void SetCornersHeight()
        {
            Random random = new Random();
            landscape[0, 0] = random.Next(0, 255*3);
            landscape[width - 1, 0] = random.Next(0, 255*3);
            landscape[0, height - 1] = random.Next(0, 255*3);
            landscape[width - 1, height - 1] = random.Next(0, 255*3);
        }

        public void SetColored(bool isColored)
        {
            if (isColored)
            {
                getHeightColor = GetHeightColor_Colored;
            } else
            {
                getHeightColor = GetHeightColor_BlackWhite;
            }
        }

        public void SetRoughness(double roughness)
        {
            this.roughness = roughness;
        }

        private Drawer.Color GetHeightColor_BlackWhite(int height)
        {
            if (height < 0)
            {
                height = 0;
            }
            if (height > 765)
            {
                height = 765;
            }

            int hue = (int)(height / 765.0 * 255);

            return new Drawer.Color(hue, hue, hue);
        }

        private Drawer.Color GetHeightColor_Colored(int height)
        {
            if (height < 0)
            {
                height = 0;
            }
            if (height > 765)
            {
                height = 765;
            }

            if (height <= 255)
            {
                var c = HslToRgb(230, 0.90, (height / 255.0 * 40 + 20) / 100.0);
                return new Drawer.Color(c.Item3, c.Item2, c.Item1);
            } else if (height <= 510)
            {
                var c = HslToRgb(100, 0.90, (40 - ((height-255) / 255.0 * 20)) / 100.0);
                return new Drawer.Color(c.Item3, c.Item2, c.Item1);
            } 
            else
            {
                var c = HslToRgb(40, 0.20, ((height-510) / 255.0 * 80 + 20) / 100.0);
                return new Drawer.Color(c.Item3, c.Item2, c.Item1);
            }
        }

        public async void Calculate(Action updateImage)
        {
            drawer.Clear();
            if (isAnimated)
            {
                await Task.Delay(10);
            }
            updateImage();
            _Calculate(0, 0, width - 1, height - 1, updateImage);

            UpdateDrawer();
        }

        private async void _Calculate(int x1, int y1, int x2, int y2, Action updateImage)
        {
            if (x2 - x1 < 2 || y2 - y1 < 2) return;

            int xCenter = (x2 + x1) / 2;
            int yCenter = (y2 + y1) / 2;

            // top
            landscape[xCenter, y1] = (landscape[x1, y1] + landscape[x2, y1]) / 2;
            // left
            landscape[x1, yCenter] = (landscape[x1, y1] + landscape[x1, y2]) / 2;
            // bottom
            landscape[xCenter, y2] = (landscape[x1, y2] + landscape[x2, y2]) / 2;
            // right
            landscape[x2, yCenter] = (landscape[x2, y1] + landscape[x2, y2]) / 2;
            // center
            landscape[xCenter, yCenter] = (landscape[x1, y1] + landscape[x1, y2] + landscape[x2, y1] + landscape[x2, y2]) / 4;
            // noise
            int length = ((x2 - x1) + (y2 - y1)) / 2;
            landscape[xCenter, yCenter] += (int)GetRandomNumber(-roughness * length, roughness * length);

            if (isAnimated)
            {
                drawer.DrawPoint(new Drawer.Point(xCenter, y1), getHeightColor(landscape[xCenter, y1]));
                drawer.DrawPoint(new Drawer.Point(x1, yCenter), getHeightColor(landscape[x1, yCenter]));
                drawer.DrawPoint(new Drawer.Point(xCenter, y2), getHeightColor(landscape[xCenter, y2]));
                drawer.DrawPoint(new Drawer.Point(x2, yCenter), getHeightColor(landscape[x2, yCenter]));
                drawer.DrawPoint(new Drawer.Point(xCenter, yCenter), getHeightColor(landscape[xCenter, yCenter]));
                
                updateImage();

                await Task.Delay(4000);
            }

            // left top
            _Calculate(x1, y1, xCenter, yCenter, updateImage);
            // right top
            _Calculate(xCenter, y1, x2, yCenter, updateImage);
            // left bottom
            _Calculate(x1, yCenter, xCenter, y2, updateImage);
            // right bottom
            _Calculate(xCenter, yCenter, x2, y2, updateImage);
        }

        public void Clear()
        {
            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                {
                    landscape[i, j] = 255;
                }
        }

        public void UpdateDrawer()
        {
            for (int i = 0; i < width; i++)
            for (int j = 0; j < height; j++)
                {
                    drawer.DrawPoint(new Drawer.Point(i, j), getHeightColor(landscape[i, j]));
                }
        }

        private double GetRandomNumber(double minimum, double maximum)
        {
            Random random = new Random();
            return random.NextDouble() * (maximum - minimum) + minimum;
        }

        public (int, int, int) HslToRgb(double h, double s, double l)
        {
            byte r = 0;
            byte g = 0;
            byte b = 0;

            if (s == 0)
            {
                r = g = b = (byte)(l * 255);
            }
            else
            {
                double v1, v2;
                double hue = (double)h / 360;

                v2 = (l < 0.5) ? (l * (1 + s)) : ((l + s) - (l * s));
                v1 = 2 * l - v2;

                r = (byte)(255 * HueToRGB(v1, v2, hue + (1.0 / 3)));
                g = (byte)(255 * HueToRGB(v1, v2, hue));
                b = (byte)(255 * HueToRGB(v1, v2, hue - (1.0 / 3)));
            }

            return (r, g, b);
        }

        public double HueToRGB(double v1, double v2, double vH)
        {
            if (vH < 0)
                vH += 1;

            if (vH > 1)
                vH -= 1;

            if ((6 * vH) < 1)
                return (v1 + (v2 - v1) * 6 * vH);

            if ((2 * vH) < 1)
                return v2;

            if ((3 * vH) < 2)
                return (v1 + (v2 - v1) * ((2.0 / 3) - vH) * 6);

            return v1;
        }
    }
}
