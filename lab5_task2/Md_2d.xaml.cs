﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using MD;

namespace lab5_task2
{
    /// <summary>
    /// Логика взаимодействия для Md_2d.xaml
    /// </summary>
    public partial class Md_2d : Window
    {
        MD.MidpointDisplacement midpointDisplacement;
        public Md_2d()
        {
            InitializeComponent();

            midpointDisplacement = new MD.MidpointDisplacement(512, 512);

            Roughness.Text = (1.2).ToString();

            midpointDisplacement.SetCornersHeight();
            midpointDisplacement.SetRoughness(1.2);
            midpointDisplacement.Calculate(UpdateImage);
            UpdateImage();
        }

        private void UpdateImage()
        {
            Image.Source = midpointDisplacement.Bitmap;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9,]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            midpointDisplacement.IsAnimated = (bool)IsAnimated.IsChecked;
            midpointDisplacement.Clear();
            midpointDisplacement.SetRoughness(double.Parse(Roughness.Text));
            midpointDisplacement.SetColored((bool)IsColored.IsChecked);
            midpointDisplacement.Calculate(UpdateImage);
            UpdateImage();
        }
    }
}
