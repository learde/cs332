﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Drawer;


namespace lab5_task3
{

    public partial class MainWindow : Window
    {

        private const int THRESHOLD = 25;

        private enum Action
        {
            NONE,
            DRAW,
            DELETE,
            MOVE,
        }

        Drawer.Drawer _drawer;
        private readonly List<Drawer.Point> _points = new();
        Action _action = Action.NONE;

        private readonly List<Button> _buttons = new();

        private Action<Drawer.Point> _pointDrawn;
        private Action<Drawer.Point> _pointDelete;


        private BezierCurve _curve;


        // editing
        bool _isEditing = false;
        private bool _isDragging = false;

        private Drawer.Point _movingPoint;

        public MainWindow()
        {
            InitializeComponent();
            _drawer = new Drawer.Drawer(800, 650);
            UpdateImage();
            ContentRendered += OnContentRendered;
            _curve = new BezierCurve();
            _curve.Changed += OnChanged;
        }

        private void OnChanged()
        {
            if (_points.Count > 2)
                Redraw();
        }

        private void OnContentRendered(object? sender, EventArgs e)
        {
            Utilities.Find(this, _buttons);
        }

        void DisableInterface()
        {
            _buttons.ForEach(button => button.IsEnabled = false);
        }

        void EnableInterface()
        {
            _buttons.ForEach(button =>
            {
                button.IsEnabled = true;
                button.BorderBrush = new SolidColorBrush(System.Windows.Media.Color.FromRgb(112, 112, 112));
            });
        }

        void HightlightButton(Button btn)
        {
            btn.IsEnabled = true;
            btn.BorderBrush = Brushes.Red;
        }

        void StartImageEditing(Action action)
        {
            _isEditing = true;
            _action = action;
        }

        void StopImageEditing()
        {
            _isEditing = false;
            _action = Action.NONE;
        }

        void UpdateImage()
        {
            Image.Source = _drawer.Bitmap;
        }

        private void DefinePointButton_Click(object sender, RoutedEventArgs e)
        {
            DisableInterface();
            HightlightButton(DefinePointButton);
            StartImageEditing(Action.DRAW);
        }

        private void DeletePointButton_Click(object sender, RoutedEventArgs e)
        {
            if (_points.Count() == 0)
                return;
            DisableInterface();
            HightlightButton(DeletePointButton);
            StartImageEditing(Action.DELETE);
        }

        private void MovePointButton_Click(object sender, RoutedEventArgs e)
        {
            if (_points.Count() == 0)
                return;
            DisableInterface();
            HightlightButton(MovePointButton);
            StartImageEditing(Action.MOVE);
        }


        private void DrawPoint(int x, int y)
        {
            var point = new Drawer.Point(x, y);
            _pointDrawn?.Invoke(point);
            _drawer.DrawPoint(point, Drawer.Colors.Blue, 6);
            _points.Add(point);
            _curve.AddControlPoint(point);
            EnableInterface();
            StopImageEditing();
        }


        private Drawer.Point? DetectPoint(int x, int y)
        {
            var point = new Drawer.Point(x, y);
            var target = _points.OrderBy(p => p.Distance(point)).First();
            if (target.Distance(point) > THRESHOLD)
                return null;
            return target;
        }


        private void DeletePoint(int x, int y)
        {
            var target = DetectPoint(x, y);
            if (target == null)
                return;
            RemovePoint(target);
            _pointDelete?.Invoke(target);
            Redraw();
            EnableInterface();
            StopImageEditing();
        }

        private void RemovePoint(Drawer.Point point)
        {
            _points.Remove(point);
            _curve.RemoveControlPoint(point);
        }

        private void MovePoint(int x, int y)
        {
            var target = DetectPoint(x, y);
            if (target == null)
                return;
            _movingPoint = target;
            _isDragging = true;
        }

        private void Redraw()
        {
            _drawer.Clear();
            _points.ForEach(x => _drawer.DrawPoint(x, Drawer.Colors.Blue, 6));
            _curve.Calculate();
            _drawer.DrawCurve(_curve, Drawer.Colors.Black);
            UpdateImage();
        }


        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!_isEditing)
                return;

            var position = e.GetPosition(Image);
            var x = (int)position.X;
            var y = (int)position.Y;

            switch (_action)
            {
                case Action.DRAW:
                    DrawPoint(x, y);
                    break;
                case Action.DELETE:
                    DeletePoint(x, y);
                    break;
                case Action.MOVE:
                    MovePoint(x, y);
                    break;
            }
            UpdateImage();
        }


        private void Image_MouseUp(object sender, RoutedEventArgs e)
        {
            if (_action == Action.MOVE)
            {
                _isDragging = false;
                EnableInterface();
                StopImageEditing();
            }

        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            _points.Clear();
            _drawer.Clear();
            _curve.Clear();
            UpdateImage();
        }

        private void Image_MouseMove(object sender, MouseEventArgs e)
        {
            if (!_isEditing)
                return;

            var position = e.GetPosition(Image);
            var x = (int)position.X;
            var y = (int)position.Y;

            if (_action == Action.MOVE && _isDragging)
            {
                _movingPoint.X = x;
                _movingPoint.Y = y;
                Redraw();
            }

            UpdateImage();
        }
    }
}
