﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace lab5_task1
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public class PointF
        {
            float x;
            float y;

            public PointF(float x, float y)
            {
                this.X = x;
                this.Y = y;
            }

            public float X { get => x; set => x = value; }
            public float Y { get => y; set => y = value; }
        }

        WriteableBitmap writeableBitmap;
        RenderTargetBitmap renderTargetBitmap;
        string fileName = "..//..//..//L-systems//КриваяКоха.txt";
        int generation = 0, randFrom = 0, randTo = 0;
        List<string> LSystem = new List<string>();
        int windowWidth, windowHeight;
        List<Tuple<PointF, PointF>> points = new List<Tuple<PointF, PointF>>();

        public MainWindow()
        {
            InitializeComponent();
            windowWidth = 640;
            windowHeight = 414;
            writeableBitmap = new WriteableBitmap(windowWidth, windowHeight, 96, 96, PixelFormats.Bgra32, null);
            renderTargetBitmap = new RenderTargetBitmap(windowWidth, windowHeight, 96, 96, PixelFormats.Default);
            ResetBitmap();
        }

        private void generationBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char el = e.Text[0];
            if (!Char.IsDigit(el) && el != (char)Key.Back)
                e.Handled = true;
        }

        private void randFromButton_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char el = e.Text[0];
            if (!Char.IsDigit(el) && el != (char)Key.Back && el != '-') 
                e.Handled = true;
        }

        private void randToButton_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            char el = e.Text[0];
            if (!Char.IsDigit(el) && el != (char)Key.Back && el != '-') 
                e.Handled = true;
        }

        private void systemButton_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int ind = systemButton.SelectedIndex;
            switch (ind)
            {
                case 0:
                    fileName = "..//..//L-systems//КриваяКоха.txt";
                    break;
                case 1:
                    fileName = "..//..//L-systems//СнежинкаКоха.txt";
                    break;
                case 2:
                    fileName = "..//..//L-systems//ТреугольникСерпинского.txt";
                    break;
                case 3:
                    fileName = "..//..//L-systems//КоверСерпинского.txt";
                    break;
                case 4:
                    fileName = "..//..//L-systems//ШестиугольнаяКриваяГоспера.txt";
                    break;
                case 5:
                    fileName = "..//..//L-systems//КриваяГильберта.txt";
                    break;
                case 6:
                    fileName = "..//..//L-systems//КриваяДракона.txt";
                    break;
                case 7:
                    fileName = "..//..//L-systems//ВысокоеДерево.txt";
                    break;
                case 8:
                    fileName = "..//..//L-systems//ШирокоеДерево.txt";
                    break;
                case 9:
                    fileName = "..//..//L-systems//Куст.txt";
                    break;
            }            
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            ResetBitmap();
        }

        private void drawButton_Click(object sender, RoutedEventArgs e)
        {
            ResetBitmap();
            renderTargetBitmap = new RenderTargetBitmap((int)border.ActualWidth, (int)border.ActualHeight, 96, 96, PixelFormats.Default);
            generation = generationBox.Text != "" ? Int32.Parse(generationBox.Text) : 1;
            randFrom = randFromBox.Text != "" ? Int32.Parse(randFromBox.Text) : 0;
            randTo = randToBox.Text != "" ? Int32.Parse(randToBox.Text) : 0;

            string axiom = "", direction = "";
            float rotate = 0;
            LSystem = new List<string>();
            points = new List<Tuple<PointF, PointF>>();
            try
            {
                StreamReader sr = new StreamReader(fileName);
                string line = sr.ReadLine();
                string[] parseLine = line.Split(' ');
                axiom = parseLine[0];                
                rotate = float.Parse(parseLine[1]);
                direction = parseLine[2];

                line = sr.ReadLine();
                while (line != null)
                {
                    LSystem.Add(line);
                    line = sr.ReadLine();
                }
                sr.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception: " + ex.Message);
            }
            finally
            {
                Console.WriteLine("Executing finally block.");
            }

            DrawFractal(axiom, rotate, direction);
        }

        private void DrawFractal(string rule, float rot, string dir)
        {           
            if (randFrom > randTo)
            {
                (randFrom, randTo) = (randTo, randFrom);
            }
                        
            Dictionary<char, string> systemRules = new Dictionary<char, string>();
            foreach (string line in LSystem)
                systemRules[line[0]] = line.Substring(2);

            for (int i = 0; i < generation; ++i)
            {
                string seq = "";
                foreach (char lex in rule)
                    if (systemRules.ContainsKey(lex))
                        seq += systemRules[lex];
                    else
                        seq += lex;
                rule = seq;
            }

            float angle = 0;
            
            switch (dir)
            {
                case "left":
                    angle = (float)Math.PI;
                    break;
                case "right":
                    angle = 0;
                    break;
                case "up":
                    angle = (float)(3 * Math.PI / 2);
                    break;
            }

            rot = rot * (float)Math.PI / 180; // градусы в радианы
            Stack<Tuple<PointF, float>> st = new Stack<Tuple<PointF, float>>();
            PointF point = new PointF(0, 0);
            Random rand = new Random();
            int randRotate = 0;
            int count = 0;
            Color color = Color.FromArgb(64, 0, 0, 0);
            Dictionary<PointF, Tuple<Color, float>> gr = new Dictionary<PointF, Tuple<Color, float>>();

            float width = 5;
            if (fileName == "..//..//..//L-systems//ШирокоеДерево.txt")
                width = 10;

            foreach (char lex in rule)
            {
                if (lex == 'F')
                {                    
                    float nextX = (float)(point.X + Math.Cos(angle)), nextY = (float)(point.Y + Math.Sin(angle));
                    points.Add(Tuple.Create(point, new PointF(nextX, nextY)));

                    if (fileName.Contains("Дерево") || fileName.Contains("Куст"))
                    {
                        if (count < 3)
                        {
                            width--;
                            count++;
                        }
                        gr[point] = new Tuple<Color, float>(color, width);
                    }

                    point = new PointF(nextX, nextY);
                }
                else if (lex == '[')
                {
                    st.Push(Tuple.Create(point, angle));
                    if (fileName.Contains("Дерево") || fileName.Contains("Куст"))
                    {
                        color = color.G + 40 > 255 ? Color.FromRgb(color.R, 255, color.B) : Color.FromRgb(color.R, (byte)(color.G + 40), color.B);
                        
                    }
                }
                else if (lex == ']')
                {
                    Tuple<PointF, float> tuple = st.Pop();
                    point = tuple.Item1;
                    angle = tuple.Item2;
                    if (fileName.Contains("Дерево") || fileName.Contains("Куст"))
                    {
                        color = color.G - 40 < 0 ? Color.FromRgb(color.R, 0, color.B) : Color.FromRgb(color.R, (byte)(color.G - 40), color.B);
                        
                    }
                }
                else if (lex == '-')
                {
                    randRotate = rand.Next(randFrom, randTo + 1);
                    angle -= rot + randRotate * (float)Math.PI / 180;
                }
                else if (lex == '+')
                {
                    randRotate = rand.Next(randFrom, randTo + 1);
                    angle += rot + randRotate * (float)Math.PI / 180;
                }
            }

            // находим минимум и максимум полученных точек для масштабирования
            float minX = points.Min(pointt => Math.Min(pointt.Item1.X, pointt.Item2.X)), maxX = points.Max(pointt => Math.Max(pointt.Item1.X, pointt.Item2.X));
            float minY = points.Min(pointt => Math.Min(pointt.Item1.Y, pointt.Item2.Y)), maxY = points.Max(pointt => Math.Max(pointt.Item1.Y, pointt.Item2.Y));

            // центр окна
            PointF center = new PointF((float)(border.ActualWidth / 2), (float)(border.ActualHeight / 2));
            // центр полученного фрактала
            PointF fractal = new PointF(minX + (maxX - minX) / 2, minY + (maxY - minY) / 2);
            // шаг для масштабирования
            float step = Math.Min((float)(border.ActualWidth / (maxX - minX)), (float)(border.ActualHeight / (maxY - minY)));

            List<Tuple<PointF, PointF>> scalePoints = new List<Tuple<PointF, PointF>>(points);
            // масштабируем список точек
            for (int i = 0; i < points.Count(); i++)
            {
                float scaleX = center.X + (points[i].Item1.X - fractal.X) * step,
                    scaleY = center.Y + (points[i].Item1.Y - fractal.Y) * step;
                float scaleNextX = center.X + (points[i].Item2.X - fractal.X) * step,
                    scaleNextY = center.Y + (points[i].Item2.Y - fractal.Y) * step;
                scalePoints[i] = new Tuple<PointF, PointF>(new PointF(scaleX, scaleY), new PointF(scaleNextX, scaleNextY));
            }

            DrawingVisual drawingVisual = new DrawingVisual();
            DrawingContext drawingContext = drawingVisual.RenderOpen();

            if (fileName.Contains("Дерево") || fileName.Contains("Куст"))
            {
                
                for (int i = 0; i < points.Count(); ++i)
                {
                    SolidColorBrush brush = new SolidColorBrush(gr[points[i].Item1].Item1);                   
                    drawingContext.DrawLine(new Pen(brush, gr[points[i].Item1].Item2), new Point((int)scalePoints[i].Item1.X, (int)scalePoints[i].Item1.Y), new Point((int)scalePoints[i].Item2.X, (int)scalePoints[i].Item2.Y));
                }    
            }
            else
            {
                for (int i = 0; i < points.Count(); ++i)
                    drawingContext.DrawLine(new Pen(Brushes.Black, 2), new Point((int)scalePoints[i].Item1.X, (int)scalePoints[i].Item1.Y), new Point((int)scalePoints[i].Item2.X, (int)scalePoints[i].Item2.Y));
            }

            drawingContext.Close();
            renderTargetBitmap.Render(drawingVisual);
            image.Source = renderTargetBitmap;
        }
                             
        public void ResetBitmap() 
        {
            var whiteBytes = new byte[windowWidth * windowHeight * 4];
            for (var i = 0; i < whiteBytes.Length; i += 4)
            {
                whiteBytes[i] = 255;
                whiteBytes[i + 1] = 255;
                whiteBytes[i + 2] = 255;
                whiteBytes[i + 3] = 255;
            }
            writeableBitmap.WritePixels(new Int32Rect(0, 0, windowWidth, windowHeight), whiteBytes, windowWidth * 4, 0);            
            UpdateImage();
        }

        void UpdateImage()
        {
            image.Source = writeableBitmap;
        }
    }
}
