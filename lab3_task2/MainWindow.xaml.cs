﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace lab3_task2
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Drawer.Drawer drawer;
        WriteableBitmap bitmapBefore = null;
        Point start;
        Point end;

        delegate void DrawingMethod(int x1, int y1, int x2, int y2, byte[] colorData);
        static DrawingMethod drawingMethod;

        public MainWindow()
        {
            InitializeComponent();
            drawer = new Drawer.Drawer(800,504);
            UpdateImage();
            drawingMethod = drawer.DrawLineWu;
        }

        private void WU_Click(object sender, RoutedEventArgs e)
        {
            drawingMethod = drawer.DrawLineWu;
        }

        private void Bresenham_Click(object sender, RoutedEventArgs e)
        {
            drawingMethod = drawer.DrawLineBresenham;
        }

        private void image_MouseDown(object sender, MouseButtonEventArgs e)
        {
            bitmapBefore = new WriteableBitmap(drawer.GetBitmap());
            start = e.GetPosition(image);
        }

        private void image_MouseMove(object sender, MouseEventArgs e)
        {
            if (start.X != 0 && start.Y != 0)
            {
                drawer.SetBitmap(bitmapBefore);                
                end = e.GetPosition(image);
                drawingMethod((int)start.X, (int)start.Y, (int)end.X, (int)end.Y, new byte[] { 0, 0, 0, 255 });
                UpdateImage();
            }
        }

        private void image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            start.X = 0;
            start.Y = 0;
            UpdateImage();
            bitmapBefore = null;
        }

        public void UpdateImage()
        {
            image.Source = drawer.GetBitmap();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            drawer.FillWhite();
            UpdateImage();
        }
    }
}
