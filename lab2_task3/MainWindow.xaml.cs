﻿using System;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;

namespace lab2_task3
{
    public partial class MainWindow : Window
    {
        private Bitmap originalBitmap;
        private WriteableBitmap writableBitmap;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OpenImageButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "Image Files|*.bmp;*.jpg;*.jpeg;*.png;*.gif|All Files|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                originalBitmap = new Bitmap(openFileDialog.FileName);
                writableBitmap = ConvertToWriteableBitmap(originalBitmap);
                ImageDisplay.Source = writableBitmap;
            }
        }

        private void AdjustImage_Click(object sender, RoutedEventArgs e)
        {
            if (writableBitmap != null)
            {
                if ((sender as Button).Name == "ResetImageButton")
                {
                    HueSlider.Value = 0;
                    SaturationSlider.Value = 0;
                    ValueSlider.Value = 0;
                }

                double hue = HueSlider.Value;
                double saturation = SaturationSlider.Value;
                double brightness = ValueSlider.Value;

                ApplyHSVAdjustment(hue, saturation, brightness);
            }
        }

        private void SaveImageButton_Click(object sender, RoutedEventArgs e)
        {
            if (writableBitmap != null)
            {
                Microsoft.Win32.SaveFileDialog saveFileDialog = new Microsoft.Win32.SaveFileDialog();
                saveFileDialog.Filter = "JPEG Image|*.jpg";

                if (saveFileDialog.ShowDialog() == true)
                {
                    using (var stream = new FileStream(saveFileDialog.FileName, FileMode.Create))
                    {
                        JpegBitmapEncoder encoder = new JpegBitmapEncoder();
                        encoder.Frames.Add(BitmapFrame.Create(writableBitmap));
                        encoder.Save(stream);
                    }
                }
            }
        }

        private WriteableBitmap ConvertToWriteableBitmap(Bitmap bitmap)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                bitmap.Save(memoryStream, System.Drawing.Imaging.ImageFormat.Bmp);
                memoryStream.Position = 0;

                BitmapImage bitmapImage = new BitmapImage();
                bitmapImage.BeginInit();
                bitmapImage.StreamSource = memoryStream;
                bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                bitmapImage.EndInit();

                return new WriteableBitmap(bitmapImage);
            }
        }

        private void ApplyHSVAdjustment(double hueDelta, double saturationDelta, double brightnessDelta)
        {
            if (originalBitmap != null)
            {
                Bitmap adjustedBitmap = new Bitmap(originalBitmap.Width, originalBitmap.Height);

                for (int y = 0; y < originalBitmap.Height; y++)
                {
                    for (int x = 0; x < originalBitmap.Width; x++)
                    {
                        System.Drawing.Color pixel = originalBitmap.GetPixel(x, y);
                        double h, s, v;
                        ColorToHSV(pixel, out h, out s, out v);

                        // Применяем настройки оттенка, насыщенности и яркости
                        h += hueDelta;
                        s += saturationDelta;
                        v += brightnessDelta;

                        // Обрезаем значения, чтобы они находились в допустимых пределах (0-360 для h и 0-1 для s и v)
                        h = Math.Max(0, Math.Min(360, h));
                        s = Math.Max(0, Math.Min(1, s));
                        v = Math.Max(0, Math.Min(1, v));

                        // Преобразуем обратно в RGB
                        System.Drawing.Color adjustedColor = ColorFromHSV(h, s, v);
                        adjustedBitmap.SetPixel(x, y, adjustedColor);
                    }
                }

                writableBitmap = ConvertToWriteableBitmap(adjustedBitmap);
                ImageDisplay.Source = writableBitmap;
            }
        }

        private void ColorToHSV(System.Drawing.Color color, out double hue, out double saturation, out double value)
        {
            double r = color.R / 255.0;
            double g = color.G / 255.0;
            double b = color.B / 255.0;

            double min = Math.Min(Math.Min(r, g), b);
            double max = Math.Max(Math.Max(r, g), b);

            value = max;
            double delta = max - min;

            if (max != min)
            {
                if (r == max && g >= b)
                    hue = 60 * (g - b) / delta;
                else if (r == max && g < b)
                    hue = 60 * (g - b) / delta + 360;
                else if (g == max)
                    hue = 60 * (b - r) / delta + 120;
                else
                    hue = 60 * (r - g) / delta + 240;
            }
            else
            {
                hue = 0;
            }

            if (max != 0)
                saturation = 1 - min / max;
            else
                saturation = 0;
        }

        private System.Drawing.Color ColorFromHSV(double hue, double saturation, double value)
        {
            int hi = (int)(Math.Floor(hue / 60.0)) % 6;
            double f = hue / 60.0 - Math.Floor(hue / 60.0);

            value = value * 255;
            int v = (int)(value);
            int p = (int)(value * (1 - saturation));
            int q = (int)(value * (1 - f * saturation));
            int t = (int)(value * (1 - (1 - f) * saturation));

            if (hi == 0)
                return System.Drawing.Color.FromArgb(255, v, t, p);
            else if (hi == 1)
                return System.Drawing.Color.FromArgb(255, q, v, p);
            else if (hi == 2)
                return System.Drawing.Color.FromArgb(255, p, v, t);
            else if (hi == 3)
                return System.Drawing.Color.FromArgb(255, p, q, v);
            else if (hi == 4)
                return System.Drawing.Color.FromArgb(255, t, p, v);
            else
                return System.Drawing.Color.FromArgb(255, v, p, q);
        }
    }
}
