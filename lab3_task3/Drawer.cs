﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Drawer
{
    public class Color
    {
        double blue;
        double green;
        double red;
        double alpha;

        public Color(double blue, double green, double red, double alpha = 255)
        {
            this.Blue = blue;
            this.Green = green;
            this.Red = red;
            this.Alpha = alpha;
        }

        public double Blue { get => blue; set => blue = value; }
        public double Green { get => green; set => green = value; }
        public double Red { get => red; set => red = value; }
        public double Alpha { get => alpha; set => alpha = value; }

        public Color GetDiffWith(Color other)
        {
            return new Color(Blue - other.Blue, Green - other.Green, Red - other.Red);
        }

        public Color GetSumWith(Color other)
        {
            return new Color(Blue + other.Blue, Green + other.Green, Red + other.Red);
        }

        public Color GetMultBy(double k)
        {
            return new Color(Blue * k, Green * k, Red * k);
        }

        private byte Normalize(double v)
        {
            if (v < 0) return 0;
            if (v > 255) return 255;
            return (byte)v;
        }

        public byte[] ToBgra()
        {
            return new byte[] { Normalize(Blue), Normalize(Green), Normalize(Red), Normalize(Alpha) };
        }

        static public void Swap(Color c1, Color c2)
        {
            double c1Blue = c1.Blue;
            double c1Green = c1.Green;
            double c1Red = c1.Red;

            c1.Blue = c2.Blue;
            c1.Green = c2.Green;
            c1.Red = c2.Red;
            c2.Blue = c1Blue;
            c2.Green = c1Green;
            c2.Red = c1Red;
        }
    }

    public class Point
    {
        int x;
        int y;

        public Point(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public int X { get => x; set => x = value; }
        public int Y { get => y; set => y = value; }

        public double XDouble { get => (double)x; }
        public double YDouble { get => (double)y; }

        static public void Swap(Point p1, Point p2)
        {
            int p2X = p2.X;
            int p2Y = p2.Y;

            p2.X = p1.X;
            p2.Y = p1.Y;
            p1.X = p2X;
            p1.Y = p2Y;
        }
    }

    public class Drawer
    {
        WriteableBitmap bitmap;
        int width;
        int height;

        public Drawer(int width, int height)
        {
            this.width = width;
            this.height = height;

            bitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgra32, null);
            FillWhite();
        }

        public bool IsPointInTriangle(Point p, Point p1, Point p2, Point p3)
        {
            int p1Side = (p1.Y - p2.Y) * p.X + (p2.X - p1.X) * p.Y + (p1.X * p2.Y - p2.X * p1.Y);
            int p2Side = (p2.Y - p3.Y) * p.X + (p3.X - p2.X) * p.Y + (p2.X * p3.Y - p3.X * p2.Y);
            int p3Side = (p3.Y - p1.Y) * p.X + (p1.X - p3.X) * p.Y + (p3.X * p1.Y - p1.X * p3.Y);

            return (p1Side >= 0 && p2Side >= 0 && p3Side >= 0) || (p1Side <= 0 && p2Side <= 0 && p3Side <= 0);
        }

        private void SwapTrianglePoints(Point p1, Color c1, Point p2, Color c2, Point p3, Color c3)
        {
            if (p1.Y > p2.Y)
            {
                if (p2.Y > p3.Y)
                {
                    Point.Swap(p1, p3);
                    Color.Swap(c1, c3);
                }
                else
                {
                    Point.Swap(p1, p2);
                    Color.Swap(c1, c2);
                }
            }
            if (p1.Y == p2.Y)
            {
                Point.Swap(p1, p3);
                Color.Swap(c1, c3);
            }
            if (p1.Y == p3.Y)
            {
                Point.Swap(p1, p2);
                Color.Swap(c1, c2);
            }
            if (p2.X > p3.X)
            {
                Point.Swap(p2, p3);
                Color.Swap(c2, c3);
            }
        }

        private Color InterpolateColor(double coord, double coord1, Color c1, double coord2, Color c2)
        {
            Color cc1 = c1.GetMultBy(1);
            Color cc2 = c2.GetMultBy(1);
            if (coord1 > coord2)
            {
                (coord1, coord2) = (coord2, coord1);
                Color.Swap(cc1, cc2);
            }

            double diff = coord2 - coord1;
            Color cDiff = cc2.GetDiffWith(cc1);

            return cDiff.GetMultBy(1 / diff).GetMultBy(coord - coord1).GetSumWith(cc1);
        }

        public void DrawTriangleLinear(Point p1, Color c1, Point p2, Color c2, Point p3, Color c3)
        {
            SwapTrianglePoints(p1, c1, p2, c2, p3, c3);

            int yMin = Math.Min(p1.Y, Math.Min(p2.Y, p3.Y));
            int yMax = Math.Max(p1.Y, Math.Max(p2.Y, p3.Y));
            int xMin = Math.Min(p1.X, Math.Min(p2.X, p3.X));
            int xMax = Math.Max(p1.X, Math.Max(p2.X, p3.X));

            for (int y = yMin; y <= yMax; y++)
            {
                int xLeft = xMin;
                while (!IsPointInTriangle(new Point(xLeft, y), p1, p2, p3)) xLeft++;
                int xRight = xMax;
                while (!IsPointInTriangle(new Point(xRight, y), p1, p2, p3)) xRight--;
                Color cLeft = InterpolateColor(y, p1.Y, c1, p2.Y, c2);
                Color cRight = InterpolateColor(y, p1.Y, c1, p3.Y, c3);

                for (int x = xLeft; x <= xRight; x++)
                {
                    Color c = InterpolateColor(x, xLeft, cLeft, xRight, cRight);
                    DrawPoint(x, y, c.ToBgra());
                }
            }
        }

        public void DrawTriangleVectors(Point p1, Color c1, Point p2, Color c2, Point p3, Color c3)
        {
            Point np1 = new Point(0, 0);
            Point np2 = new Point(p2.X - p1.X, p2.Y - p1.Y);
            Point np3 = new Point(p3.X - p1.X, p3.Y - p1.Y);

            if (np3.Y == 0)
            {
                Point.Swap(np2, np3);
                Color.Swap(c2, c3);
            }

            Color diff1 = c2.GetDiffWith(c1);
            Color diff2 = c3.GetDiffWith(c1);

            int xMin = Math.Min(np1.X, Math.Min(np2.X, np3.X));
            int yMin = Math.Min(np1.Y, Math.Min(np2.Y, np3.Y));
            int xMax = Math.Max(np1.X, Math.Max(np2.X, np3.X));
            int yMax = Math.Max(np1.Y, Math.Max(np2.Y, np3.Y));

            for (int y = yMin; y <= yMax; y++)
            {
                for (int x = xMin; x <= xMax; x++)
                {
                    double w1 = (y * np3.XDouble - x * np3.YDouble) / (np2.YDouble * np3.XDouble - np2.XDouble * np3.YDouble);

                    if (w1 >= 0 && w1 <= 1)
                    {
                        double w2 = (y - w1 * np2.YDouble) / np3.YDouble;

                        if (w2 >= 0 && ((w1 + w2) <= 1))
                        {
                            Color diff1w1 = diff1.GetMultBy(w1);
                            Color diff2w2 = diff2.GetMultBy(w2);
                            Color resultedColor = c1.GetSumWith(diff1w1).GetSumWith(diff2w2);
                            DrawPoint(x + p1.X, y + p1.Y, resultedColor.ToBgra());
                        }
                    }
                }
            }
        }

        private double GetFractionPart(double f)
        {
            return f - Math.Truncate(f);
        }

        private double GetRFractionPart(double f)
        {
            return 1 - GetFractionPart(f);
        }

        public void DrawLineWu(int x1, int y1, int x2, int y2, byte[] colorData)
        {
            bool steep = Math.Abs(y2 - y1) > Math.Abs(x2 - x1);

            if (steep)
            {
                (x1, y1) = (y1, x1);
                (x2, y2) = (y2, x2);
            }

            if (x1 > x2)
            {
                (x1, x2) = (x2, x1);
                (y1, y2) = (y2, y1);
            }

            double dx = x2 - x1;
            double dy = y2 - y1;
            double gradient = dx == 0 ? 1 : dy / dx;

            double xend = x1;
            double yend = y1 + gradient * (xend - x1);

            double xgap = GetRFractionPart(x1 + 0.5);
            double xpxl1 = xend;
            double ypxl1 = Math.Truncate(yend);

            if (steep)
            {
                DrawPoint(ypxl1, xpxl1, new byte[] { colorData[0], colorData[1], colorData[2], (byte)(GetRFractionPart(yend) * xgap * 255) });
                DrawPoint(ypxl1 + 1, xpxl1, new byte[] { colorData[0], colorData[1], colorData[2], (byte)(GetFractionPart(yend) * xgap * 255) });
            }
            else
            {
                DrawPoint(xpxl1, ypxl1, new byte[] { colorData[0], colorData[1], colorData[2], (byte)(GetRFractionPart(yend) * xgap * 255) });
                DrawPoint(xpxl1, ypxl1 + 1, new byte[] { colorData[0], colorData[1], colorData[2], (byte)(GetFractionPart(yend) * xgap * 255) });
            }

            double intery = yend + gradient;

            xend = x2;
            yend = y2 + gradient * (xend - x2);
            xgap = GetFractionPart(x2 + 0.5);
            double xpxl2 = xend;
            double ypxl2 = Math.Truncate(yend);

            if (steep)
            {
                DrawPoint(ypxl2, xpxl2, new byte[] { colorData[0], colorData[1], colorData[2], (byte)(GetRFractionPart(yend) * xgap * 255) });
                DrawPoint(ypxl2 + 1, xpxl2, new byte[] { colorData[0], colorData[1], colorData[2], (byte)(GetFractionPart(yend) * xgap * 255) });
            }
            else
            {
                DrawPoint(xpxl2, ypxl2, new byte[] { colorData[0], colorData[1], colorData[2], (byte)(GetRFractionPart(yend) * xgap * 255) });
                DrawPoint(xpxl2, ypxl2 + 1, new byte[] { colorData[0], colorData[1], colorData[2], (byte)(GetFractionPart(yend) * xgap * 255) });
            }

            if (steep)
            {
                for (double x = xpxl1 + 1; x < xpxl2; x++)
                {
                    DrawPoint(Math.Truncate(intery), x, new byte[] { colorData[0], colorData[1], colorData[2], (byte)(GetRFractionPart(intery) * 255) });
                    DrawPoint(Math.Truncate(intery) + 1, x, new byte[] { colorData[0], colorData[1], colorData[2], (byte)(GetFractionPart(intery) * 255) });
                    intery += gradient;
                }
            }
            else
            {
                for (double x = xpxl1 + 1; x < xpxl2; x++)
                {
                    DrawPoint(x, Math.Truncate(intery), new byte[] { colorData[0], colorData[1], colorData[2], (byte)(GetRFractionPart(intery) * 255) });
                    DrawPoint(x, Math.Truncate(intery) + 1, new byte[] { colorData[0], colorData[1], colorData[2], (byte)(GetFractionPart(intery) * 255) });
                    intery += gradient;
                }
            }
        }

        public void DrawLineBresenham(int x1, int y1, int x2, int y2, byte[] colorData)
        {
            int dy = Math.Abs(y2 - y1);
            int dx = Math.Abs(x2 - x1);

            if (dy <= dx) // gradient <= 1
            {
                int di = 2 * dy - dx;
                if (y1 > y2)
                {
                    (y1, y2) = (y2, y1);
                    (x1, x2) = (x2, x1);
                }
                int step = 1;
                if (x1 > x2)
                {
                    step = -1;
                }
                int y = y1;
                for (int x = x1; x * step <= x2 * step; x += step)
                {
                    if (di < 0)
                    {
                        di += 2 * dy;
                    }
                    else
                    {
                        y++;
                        di += 2 * (dy - dx);
                    }

                    DrawPoint(x, y, colorData);
                }
            }
            else // gradient > 1
            {
                int di = 2 * dx - dy;

                if (x1 > x2)
                {
                    (y1, y2) = (y2, y1);
                    (x1, x2) = (x2, x1);
                }
                int step = 1;
                if (y1 > y2)
                {
                    step = -1;
                }
                int x = x1;
                for (int y = y1; y * step <= y2 * step; y += step)
                {
                    if (di < 0)
                    {
                        di = di + 2 * dx;
                    }
                    else
                    {
                        x++;
                        di = di + 2 * (dx - dy);
                    }

                    DrawPoint(x, y, colorData);
                }
            }
        }

        public void DrawLinePrimitive(int x1, int y1, int x2, int y2, byte[] colorData)
        {
            int deltaX = x2 - x1;
            int deltaY = y2 - y1;
            double k = (double)deltaY / (double)deltaX;

            bool isVertical = k > 1;

            int limit = isVertical ? deltaY : deltaX;

            bool isDirectionToRight = limit > 0;

            for (int i = 0; isDirectionToRight ? i <= limit : i >= limit; i += isDirectionToRight ? 1 : -1)
            {
                int x = x1 + (isVertical ? (int)(i / k) : i);
                int y = y1 + (isVertical ? i : (int)(i * k));
                DrawPoint(x, y, colorData);
            }
        }

        public void DrawPoint(int x, int y, byte[] colorData)
        {
            Int32Rect rect = new Int32Rect(x, y, 1, 1);
            bitmap.WritePixels(rect, colorData, 4, 0);
        }

        public void DrawPoint(double x, double y, byte[] colorData)
        {
            DrawPoint((int)x, (int)y, colorData);
        }

        public void FillWhite()
        {
            var whiteBytes = new byte[width * height * 4];
            for (var i = 0; i < whiteBytes.Length; i += 4)
            {
                whiteBytes[i] = 255;
                whiteBytes[i + 1] = 255;
                whiteBytes[i + 2] = 255;
                whiteBytes[i + 3] = 255;
            }
            bitmap.WritePixels(new Int32Rect(0, 0, width, height), whiteBytes, width * 4, 0);
        }

        public WriteableBitmap GetBitmap()
        {
            return bitmap;
        }

        public void SetBitmap(WriteableBitmap bm)
        {
            this.bitmap = new WriteableBitmap(bm);
        }
    }
}
