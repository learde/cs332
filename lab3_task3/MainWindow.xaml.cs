﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Drawer;

namespace lab3_task3
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Drawer.Drawer drawer;
        
        public MainWindow()
        {
            InitializeComponent();

            drawer = new Drawer.Drawer((int)Width, (int)Height);

            Drawer.Point p1 = new Drawer.Point(200, 350);
            Drawer.Color c1 = new Drawer.Color(255, 0, 0);

            Drawer.Point p2 = new Drawer.Point(150, 150);
            Drawer.Color c2 = new Drawer.Color(0, 255, 0);

            Drawer.Point p3 = new Drawer.Point(350, 150);
            Drawer.Color c3 = new Drawer.Color(0, 0, 255);

            drawer.DrawTriangleVectors(p1, c1, p2, c2, p3, c3);
            p1.X += 250;
            p2.X += 250;
            p3.X += 250;
            drawer.DrawTriangleLinear(p1, c1, p2, c2, p3, c3);

            UpdateImage();
        }

        public void UpdateImage()
        {
            image.Source = drawer.GetBitmap();
        }
    }
}
