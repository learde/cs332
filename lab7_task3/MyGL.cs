﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;

namespace lab6
{
    class MyGL
    {
        MyBitmap myBitmap;

        int width; int height;

        List<Polyhedron> polyhedrons;

        public List<Polyhedron> Polyhedrons { get { return polyhedrons; } }

        CameraLookAt camera;
        Matrix projectionMatrix;
        bool isParallel;

        public bool ShouldCutNonFacial { get; set; }
        public bool ShouldCutInvisible { get; set; }
        public bool ShouldDrawNormals { get; set; }
        public bool ShouldDrawSkeleton { get; set; }
        public bool ShouldFill { get; set; }

        public bool HasLight { get; set; }
        
        public CameraLookAt Camera { get { return camera; } }

        public MyGL(int width, int height)
        {
            ShouldDrawSkeleton = true;

            this.width = width;
            this.height = height;   
 
            myBitmap = new MyBitmap(width, height);

            camera = new CameraLookAt();

            polyhedrons = new List<Polyhedron> {};

            SetPerspective();
        }

        public void ResetCamera()
        {
            camera = new CameraLookAt();
            Draw();
        }

        public void SetPerspective()
        {
            projectionMatrix = camera.GetPerspectiveMatrix(Math.PI / 4, width / height, 0.1, 2.0);
            isParallel = false;
        }

        public void SetOrtho()
        {
            projectionMatrix = camera.GetOrthoMatrix(-1, 1, -1, 1, 0.1, 2.0);
            isParallel = true;
        }

        public void SetPolyhedron(Polyhedron p)
        {
            polyhedrons.Add(p);
            Draw();
        }

        public void RotateShape(int shapeInd, string axis, double angle)
        {
            polyhedrons[shapeInd].Rotate(axis, angle);
            Draw();
        }

        public void TranslateShape(int shapeInd, double x, double y, double z)
        {
            polyhedrons[shapeInd].Translate(x, y, z);
            Draw();
        }

        public void MirrorShape(int shapeInd, string axis)
        {
            polyhedrons[shapeInd].Mirror(axis);
            Draw();
        }

        public void ScaleShape(int shapeInd, double c)
        {
            polyhedrons[shapeInd].Scale(c, c, c);
            Draw();
        }

        public void Draw()
        {
            myBitmap.Clear();

            myBitmap.EnableZBuffer(ShouldCutInvisible);
            if (ShouldCutInvisible)
            {
                myBitmap.ClearZBuffer(-300 * 100);

            }

            polyhedrons.ForEach(polyhedron => DrawShape(polyhedron));
        }

        public void DrawShape(Polyhedron polyhedron)
        {
            if (polyhedron == null) return;

            var modelMatrix = Matrix.Identity(); 
            var normalMatrix = Matrix.Identity();
            var viewMatrix = camera.GetViewMatrix();

            modelMatrix.Scale(polyhedron.Scalation[0], polyhedron.Scalation[1], polyhedron.Scalation[2]);

            modelMatrix.RotateX(polyhedron.Rotation[0]);
            modelMatrix.RotateY(polyhedron.Rotation[1]);
            modelMatrix.RotateZ(polyhedron.Rotation[2]);

            normalMatrix.RotateX(polyhedron.Rotation[0]);
            normalMatrix.RotateY(polyhedron.Rotation[1]);
            normalMatrix.RotateZ(polyhedron.Rotation[2]);

            modelMatrix.Translate(polyhedron.Translation[0], polyhedron.Translation[1], polyhedron.Translation[2]);

            var invertMatrix = Matrix.Transpose(Matrix.Invert(modelMatrix));

            LightSource ls = new LightSource(new Vector(3, 0, 0, -1));

            Debug.WriteLine("Drawing start...");

            polyhedron.Polygons.ForEach(polygon =>
            {
                var mvEdges = new List<Edge>();

                polygon.Edges.ForEach(edge =>
                {
                    var mp2 = Matrix.ToPoint(Matrix.Multiply(Matrix.FromPoint(edge.P2), modelMatrix));
                    //mp2.Normal = edge.P2.Normal;
                    mp2.Normal = Matrix.ToVector(Matrix.Multiply(Matrix.FromVector(edge.P2.Normal), normalMatrix));
                    var mp1 = Matrix.ToPoint(Matrix.Multiply(Matrix.FromPoint(edge.P1), modelMatrix));
                    mp1.Normal = Matrix.ToVector(Matrix.Multiply(Matrix.FromVector(edge.P1.Normal), normalMatrix));
                    //mp1.Normal = edge.P1.Normal;

                    var p2 = Matrix.ToPoint(Matrix.Multiply(Matrix.FromPoint(mp2), viewMatrix));
                    p2.U = edge.P2.U;
                    p2.V = edge.P2.V;
                    p2.Color = edge.P2.Color;
                    p2.Normal = mp2.Normal; //Matrix.ToVector(Matrix.Multiply(Matrix.FromVector(mp2.Normal), viewMatrix));

                    var p1 = Matrix.ToPoint(Matrix.Multiply(Matrix.FromPoint(mp1), viewMatrix));
                    p1.U = edge.P1.U;
                    p1.V = edge.P1.V;
                    p1.Color = edge.P1.Color;
                    p1.Normal = mp1.Normal; //Matrix.ToVector(Matrix.Multiply(Matrix.FromVector(mp1.Normal), viewMatrix));

                    mvEdges.Add(new Edge(p1, p2));
                });

                var mvPolygon = new Polygon(mvEdges);
                mvPolygon.Color = polygon.Color;

                Debug.WriteLine(mvPolygon);

                var shouldDraw = !ShouldCutNonFacial || IsNonFacialPolygon(mvPolygon);

                if (shouldDraw)
                {
                    var mvpEdges = new List<Edge>();

                    mvPolygon.Edges.ForEach(edge =>
                    {
                        var p2 = Matrix.ToPoint(Matrix.Multiply(Matrix.FromPoint(edge.P2).Scale(1.0 / 300, 1.0 / 300, 1.0 / 300), projectionMatrix).Scale(300, 300, 300));
                        p2.U = edge.P2.U;
                        p2.V = edge.P2.V;
                        p2.Normal = edge.P2.Normal; //Matrix.ToVector(Matrix.Multiply(Matrix.FromVector(edge.P2.Normal).Scale(1.0 / 300, 1.0 / 300, 1.0 / 300), projectionMatrix).Scale(300, 300, 300));
                        p2.Color = edge.P2.Color;
                        p2.Color = LightingCalculator.CalculateLambertian(p2, ls);

                        var p1 = Matrix.ToPoint(Matrix.Multiply(Matrix.FromPoint(edge.P1).Scale(1.0 / 300, 1.0 / 300, 1.0 / 300), projectionMatrix).Scale(300, 300, 300));
                        p1.U = edge.P1.U;
                        p1.V = edge.P1.V;
                        p1.Normal = edge.P1.Normal; //Matrix.ToVector(Matrix.Multiply(Matrix.FromVector(edge.P1.Normal).Scale(1.0 / 300, 1.0 / 300, 1.0 / 300), projectionMatrix).Scale(300, 300, 300));
                        p1.Color = edge.P1.Color;
                        p1.Color = LightingCalculator.CalculateLambertian(p1, ls);

                        mvpEdges.Add(new Edge(p1, p2));
                    });

                    var mvpPolygon = new Polygon(mvpEdges);
                    mvpPolygon.Color = polygon.Color;

                    Debug.WriteLine(mvpPolygon);

                    if (ShouldDrawSkeleton)
                    {
                        mvpPolygon.Edges.ForEach(edge =>
                        {
                            myBitmap.DrawLine(edge, Colors.Black);
                        });
                    }

                    if (ShouldDrawNormals)
                    {
                        var polygonNormal = mvpPolygon.GetNormal();
                        var pc = mvpPolygon.GetCenter();

                        myBitmap.DrawLine(pc.X + 40 * polygonNormal[0], pc.Y + 40 * polygonNormal[1], pc.Z + 40 * polygonNormal[2], pc.X, pc.Y, pc.Z, Colors.Red);
                    }

                    if (ShouldFill && polyhedron.Texture == null)
                    {
                        myBitmap.FillTriangle(mvpPolygon.Points[0], mvpPolygon.Points[0].Color, mvpPolygon.Points[1], mvpPolygon.Points[1].Color, mvpPolygon.Points[2], mvpPolygon.Points[2].Color);
                    } else if (ShouldFill && polyhedron.Texture != null)
                    {
                        myBitmap.FillTriangleTexture(mvpPolygon.Points[0], mvpPolygon.Points[1], mvpPolygon.Points[2], polyhedron.Texture, isParallel);
                    }
                }
            });

            Debug.WriteLine("Drawing stop");
        }

        public bool IsNonFacialPolygon(Polygon polygon)
        { 
            var firstPoint = polygon.Points[0];
            var normal = polygon.GetNormal();

            if (isParallel)
            {
                var cameraCenter = new Vector(3, 0, 0, -3000);

                var cameraToCenter = new Vector(3, firstPoint.X - cameraCenter.X, firstPoint.Y - cameraCenter.Y, firstPoint.Z - cameraCenter.Z);
                cameraToCenter.Normalize();

                return Vector.DotProduct(cameraToCenter, normal) >= 0;
            }
            
            var vecFirstPoint = new Vector(3, firstPoint.X, firstPoint.Y, firstPoint.Z);

            var dotProduct = Vector.DotProduct(vecFirstPoint, normal);

            return dotProduct >= 0;
        }

        public bool CheckPolygonVisible(Polygon polygon, Vector cameraNormal)
        {
            var polygonNormal = polygon.GetNormal();

            var angle = Vector.Angle(cameraNormal, polygonNormal);

            return angle < Math.PI / 2;
        }

        public WriteableBitmap GetBitmap()
        {
            return myBitmap.Bitmap;
        }
        public void SaveToObjFile(string filePath)
        {
            
                HashSet<Point> uniqueVertices = new HashSet<Point>();
                Dictionary<Point, int> vertexIndices = new Dictionary<Point, int>();
                int vertexIndex = 1;

                // Собираем все уникальные вершины в HashSet и создаем индексы для них
                foreach (var polygon in polyhedrons[0].Polygons)
                {
                    foreach (var edge in polygon.Edges)
                    {
                        if (!vertexIndices.ContainsKey(edge.P1))
                        {
                            vertexIndices[edge.P1] = vertexIndex++;
                            uniqueVertices.Add(edge.P1);
                        }

                        if (!vertexIndices.ContainsKey(edge.P2))
                        {
                            vertexIndices[edge.P2] = vertexIndex++;
                            uniqueVertices.Add(edge.P2);
                        }
                    }
                }

                using (StreamWriter writer = new StreamWriter(filePath))
                {
                    // Записываем вершины
                    foreach (var vertex in uniqueVertices)
                    {
                        writer.WriteLine($"v {vertex.X.ToString().Replace(',', '.')} {vertex.Y.ToString().Replace(',', '.')} {vertex.Z.ToString().Replace(',', '.')}");
                    }

                    // Записываем грани
                    foreach (var polygon in polyhedrons[0].Polygons)
                    {
                        foreach (var edge in polygon.Edges)
                        {
                            int vertexIndex1 = vertexIndices[edge.P1];
                            int vertexIndex2 = vertexIndices[edge.P2];
                            writer.WriteLine($"f {vertexIndex1} {vertexIndex2}");
                            //writer.WriteLine($"f {vertexIndex1} {vertexIndex2} {vertexIndex2}");
                    }
                    }
                }
            

        }
    
    }
}
