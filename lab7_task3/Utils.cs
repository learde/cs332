﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6
{
    static class Utils
    {
        static public double Cos(double angle)
        {
            return Math.Cos(Math.PI * angle / 180.0);
        }

        static public double Sin(double angle)
        {
            return Math.Sin(Math.PI * angle / 180.0);
        }

        static public double Hypot(params double[] args)
        {
            double sum = 0;

            for (int i = 0; i < args.Length; i++)
            {
                sum += Math.Pow(args[i], 2);
            }

            return Math.Sqrt(sum);
        }
    }
}
