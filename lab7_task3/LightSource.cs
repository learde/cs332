﻿using System.Drawing;

namespace lab6
{
    class LightSource
    {
        public Vector Direction { get; set; }

        public LightSource(Vector direction)
        {
            Direction = direction;
        }
    }
}
