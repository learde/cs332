﻿using AngouriMath;
using lab7_task3;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace lab6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        MyGL myGL;

        public MainWindow()
        {
            InitializeComponent();

            myGL = new MyGL(600, 600);

            myGL.HasLight = true;
            Projection.SelectedIndex = 1;

            parseObjFile("../../../assets/cube.obj", "Cube"/*, "../../../assets/rubik.png"*/);
            myGL.ScaleShape(myGL.Polyhedrons.Count - 1, 35);
            myGL.TranslateShape(myGL.Polyhedrons.Count - 1, 0, 0, 500);

            CurrentObject.ItemsSource = myGL.Polyhedrons;
            CurrentObject.DisplayMemberPath = "NameId";
            CurrentObject.SelectedValuePath = "Id";
            CurrentObject.SelectedValue = 1;
        }

        public void UpdateImage()
        {
            Image.Source = myGL.GetBitmap();
        }

        private void Projection_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedComboBoxItem = (TextBlock)(sender as ComboBox).SelectedItem;
            switch (selectedComboBoxItem.Text)
            {
                case "Параллельная":
                    myGL.SetOrtho();
                    break;
                case "Перспективная":
                    myGL.SetPerspective();
                    break;
            }
            myGL.Draw();
            UpdateImage();
        }

        private void ButtonCube_Click(object sender, RoutedEventArgs e)
        {
            parseObjFile("../../../assets/cube.obj", "Cube", "../../../assets/rubik.png");
            myGL.ScaleShape(myGL.Polyhedrons.Count - 1, 35);
            myGL.TranslateShape(myGL.Polyhedrons.Count - 1, 0, 0, 500);
        }

        private void ButtonTetrahedron_Click(object sender, RoutedEventArgs e)
        {
            parseObjFile("../../../assets/tetrahedron.obj", "Tetrahedron", "../../../assets/pyramid.jpg");
            myGL.ScaleShape(myGL.Polyhedrons.Count - 1, 0.4);
            myGL.TranslateShape(myGL.Polyhedrons.Count - 1, 0, 0, 500);
        }

        private void ButtonOctahedron_Click(object sender, RoutedEventArgs e)
        {
            parseObjFile("../../../assets/octahedron.obj", "Octahedron", "../../../assets/diamond.jpg");
            myGL.ScaleShape(myGL.Polyhedrons.Count - 1, 35);
            myGL.TranslateShape(myGL.Polyhedrons.Count - 1, 0, 0, 500);
        }

        private void ButtonIcosahedron_Click(object sender, RoutedEventArgs e)
        {
            parseObjFile("../../../assets/icosahedron.obj", "Icosahedron");
            myGL.ScaleShape(myGL.Polyhedrons.Count - 1, 35);
            myGL.TranslateShape(myGL.Polyhedrons.Count - 1, 0, 0, 500);
        }

        private void ButtonDodecahedron_Click(object sender, RoutedEventArgs e)
        {
            parseObjFile("../../../assets/dodecahedron.obj", "Dodecahedron");
            myGL.ScaleShape(myGL.Polyhedrons.Count - 1, 35);
            myGL.TranslateShape(myGL.Polyhedrons.Count - 1, 0, 0, 500);
        }

        private void ButtonRotate_Click(object sender, RoutedEventArgs e)
        {
            double r;

            if (!double.TryParse(RotateAngle.Text, out r)) r = 0;

            myGL.RotateShape(CurrentObject.SelectedIndex, RotateAxis.Text, r);
            UpdateImage();
        }

        private void ButtonTranslate_Click(object sender, RoutedEventArgs e)
        {
            double x;
            double y;
            double z;

            if (!double.TryParse(TranslateX.Text, out x)) x = 0;
            if (!double.TryParse(TranslateY.Text, out y)) y = 0;
            if (!double.TryParse(TranslateZ.Text, out z)) z = 0;

            myGL.TranslateShape(CurrentObject.SelectedIndex, x, y, z);
            UpdateImage();
        }

        private void ButtonMirror_Click(object sender, RoutedEventArgs e)
        {
            myGL.MirrorShape(CurrentObject.SelectedIndex, MirrorAxis.Text);
            UpdateImage();
        }

        private void ButtonScale_Click(object sender, RoutedEventArgs e)
        {
            double c;

            if (!double.TryParse(ScaleCoeff.Text, out c)) c = 1;

            myGL.ScaleShape(CurrentObject.SelectedIndex, c);
            UpdateImage();
        }

        private void parseObjFile(string filename, string name = "Object", string texturePath = null)
        {
            ObjParser objParser = new ObjParser(filename);

            List<Polygon> polygons = new List<Polygon>();

            foreach (var polygon in objParser.Polygons)
            {
                List<Edge> edges = new List<Edge>();
                for (int i = 0; i < polygon.Points.Count; i++)
                {
                    edges.Add(new Edge(polygon.Points[i], polygon.Points[(i + 1) % polygon.Points.Count]));
                }

                polygons.Add(new Polygon(edges));
            }

            Polyhedron loadedPolyhedron = new Polyhedron(polygons);
            loadedPolyhedron.Name = name;

            if (texturePath != null)
            {
                loadedPolyhedron.Texture = new System.Drawing.Bitmap(texturePath);
            }

            myGL.SetPolyhedron(loadedPolyhedron);

            var t = CurrentObject.SelectedValue;
            CurrentObject.ItemsSource = null;
            CurrentObject.ItemsSource = myGL.Polyhedrons;
            CurrentObject.SelectedValue = t;
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Obj files (*.obj)|*.obj|All files (*.*)|*.*";

            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    string fileName = openFileDialog.FileName;

                    // Используем парсер для извлечения вершин и полигонов из файла
                    ObjParser objParser = new ObjParser(fileName);

                    // Создаем новый Polyhedron и устанавливаем его в myGL
                    List<Edge> edges = new List<Edge>();

                    foreach (var polygon in objParser.Polygons)
                    {
                        for (int i = 0; i < polygon.Points.Count; i++)
                        {
                            int nextIndex = (i + 1) % polygon.Points.Count;
                            edges.Add(new Edge(polygon.Points[i], polygon.Points[nextIndex]));
                        }
                    }

                    Polyhedron loadedPolyhedron = new Polyhedron(new List<Polygon>() { new Polygon(edges) });
                    myGL.SetPolyhedron(loadedPolyhedron);

                
                    UpdateImage();
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Error loading OBJ file: {ex.Message}", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Invalid file selected.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            // Создаем диалог сохранения файла
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Obj files (*.obj)|*.obj|All files (*.*)|*.*";

            // Открываем диалог сохранения файла и проверяем, был ли файл выбран
            if (saveFileDialog.ShowDialog() == true)
            {
                try
                {
                    string filePath = saveFileDialog.FileName;

                    // Сохраняем текущий объект myGL в файл формата OBJ
                    myGL.SaveToObjFile(filePath);

                    MessageBox.Show("Файл успешно сохранен", "Успех", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show($"Ошибка при сохранении файла: {ex.Message}", "Ошибка", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void Image_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.W:
                    myGL.Camera.Forward();
                    myGL.Draw();
                    e.Handled = true;
                    break;
                case Key.S:
                    myGL.Camera.Backward();
                    myGL.Draw();
                    e.Handled = true;
                    break;
                case Key.Space:
                    myGL.Camera.Up();
                    myGL.Draw();
                    e.Handled = true;
                    break;
                case Key.LeftShift:
                case Key.RightShift:
                    myGL.Camera.Down();
                    myGL.Draw();
                    e.Handled = true;
                    break;
                case Key.A:
                    myGL.Camera.Left();
                    myGL.Draw();
                    e.Handled = true;
                    break;
                case Key.D:
                    myGL.Camera.Right();
                    myGL.Draw();
                    e.Handled = true;
                    break;
                case Key.Right:
                    myGL.Camera.RotateRight();
                    myGL.Draw();
                    e.Handled = true;
                    break;
                case Key.Left:
                    myGL.Camera.RotateLeft();
                    myGL.Draw();
                    e.Handled = true;
                    break;
                case Key.Up:
                    myGL.Camera.RotateUp();
                    myGL.Draw();
                    e.Handled = true;
                    break;
                case Key.Down:
                    myGL.Camera.RotateDown();
                    myGL.Draw();
                    e.Handled = true;
                    break;
            }
        }

        private void Image_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Keyboard.ClearFocus();
            Keyboard.Focus(Image);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Keyboard.Focus(Image);
        }

        private void ButtonCameraReset_Click(object sender, RoutedEventArgs e)
        {
            myGL.ResetCamera();
        }

        private void CheckBoxNonFacial_IsCheckedChanged(object sender, RoutedEventArgs e)
        {
            myGL.ShouldCutNonFacial = (bool)NonFacial.IsChecked;
            myGL.Draw();
        }

        private void CheckBoxNormals_Checked(object sender, RoutedEventArgs e)
        {
            myGL.ShouldDrawNormals = (bool)Normals.IsChecked;
            myGL.Draw();
        }

        private void CheckBoxFill_CheckedChange(object sender, RoutedEventArgs e)
        {
            myGL.ShouldFill = (bool)Fill.IsChecked;
            myGL.Draw();
        }

        private void Skeleton_Checked(object sender, RoutedEventArgs e)
        {
            myGL.ShouldDrawSkeleton = !(bool)Skeleton.IsChecked;
            myGL.Draw();
        }

        private void Invisible_Checked(object sender, RoutedEventArgs e)
        {
            myGL.ShouldCutInvisible = (bool)Invisible.IsChecked;
            myGL.Draw();
        }
    }
}
