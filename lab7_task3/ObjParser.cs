﻿using lab6;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace lab7_task3
{
    class ObjPolygon
    {
        public List<Point> Points { get; set; }
        
        public ObjPolygon()
        {
            Points = new List<Point>();
        }
    }

    class ObjParser
    {
        public List<lab6.Point> Vertices { get; private set; }
        public List<ObjPolygon> Polygons { get; private set; }
        public List<lab6.Point> Textures { get; private set; }
        public List<Vector> Normals { get; private set; }

        Color color;

        public ObjParser(string filePath)
        {
            Vertices = new List<lab6.Point>();
            Textures = new List<lab6.Point>();
            Normals = new List<lab6.Vector>();
            Polygons = new List<ObjPolygon>();

            Random random = new Random();

            //color = Color.FromRgb((byte)random.Next(256), (byte)random.Next(256), (byte)random.Next(256));
            color = Colors.Red;

            try
            {
                ParseFile(filePath);
            }
            catch (Exception ex)
            {
                // Обработка ошибок парсинга файла
                Console.WriteLine($"Error parsing OBJ file: {ex.Message}");
            }
        }

        private void ParseFile(string filePath)
        {
            using (StreamReader reader = new StreamReader(filePath))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    line = line.Trim();
                    if (string.IsNullOrEmpty(line))
                        continue;

                    string[] parts = line.Split(' ');

                    switch (parts[0])
                    {
                        case "v":
                            ParseVertex(parts);
                            break;
                        case "vt":
                            ParseTextures(parts);
                            break;
                        case "vn":
                            ParseNormals(parts);
                            break;
                        case "f":
                            ParseFace(parts);
                            break;
                    }
                }
            }
        }

        private void ParseVertex(string[] parts)
        {
            double x = double.Parse(parts[1], CultureInfo.InvariantCulture);
            double y = double.Parse(parts[2], CultureInfo.InvariantCulture);
            double z = double.Parse(parts[3], CultureInfo.InvariantCulture);
            Vertices.Add(new lab6.Point(x, y, z));
        }

        private void ParseNormals(string[] parts)
        {
            double x = double.Parse(parts[1], CultureInfo.InvariantCulture);
            double y = double.Parse(parts[2], CultureInfo.InvariantCulture);
            double z = double.Parse(parts[3], CultureInfo.InvariantCulture);
            Normals.Add(new Vector(3, x, y, z));
        }

        private void ParseTextures(string[] parts)
        {
            double u = double.Parse(parts[1], CultureInfo.InvariantCulture);
            double v = double.Parse(parts[2], CultureInfo.InvariantCulture);
            Textures.Add(new lab6.Point(0, 0, 0, u, v));
        }

        private Point GetPoint(string[] indices, Color color)
        {
            var verticle = Vertices[int.Parse(indices[0]) - 1];
            var point = new Point(verticle);
            point.Color = color;
            if (indices.Length > 1)
            {
                var texture = Textures[int.Parse(indices[1]) - 1];
                point.U = texture.U;
                point.V = texture.V;
            }
            if (indices.Length > 2)
            {
                var normal = Normals[int.Parse(indices[2]) - 1];
                point.Normal = normal;
            }

            return point;
        }

        private void ParseFace(string[] parts)
        {
            int numPolygons = parts.Length - 3;
            var random = new Random();
           
            for (int i = 1; i <= numPolygons; i++)
            {
                var polygon = new ObjPolygon();

                var p1 = GetPoint(parts[1].Split('/'), color);
                var p2 = GetPoint(parts[i + 1].Split('/'), color);
                var p3 = GetPoint(parts[i + 2].Split('/'), color);

                polygon.Points.Add(p1);
                polygon.Points.Add(p2);
                polygon.Points.Add(p3);

                Polygons.Add(polygon);
            }
        }
        
    }
}
