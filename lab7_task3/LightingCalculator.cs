﻿using System;
using System.Windows.Media;

namespace lab6
{
     class LightingCalculator
    {
        public static Color CalculateLambertian(Point point, LightSource lightSource)
        {
            // Рассчет освещения по модели Ламберта
            Vector lightDirection = lightSource.Direction; // Нормализуем направление света
            Vector normal = point.Normal; // Нормализуем вектор нормали

            double dotProduct = Vector.DotProduct(normal, lightDirection);
            double intensity = Math.Max(dotProduct, 0.0);

            // Умножаем цвет света на интенсивность для получения диффузного цвета
            Color diffuseColor = Color.FromRgb(
                (byte)(point.Color.R * intensity),
                (byte)(point.Color.G * intensity),
                (byte)(point.Color.B * intensity)
            );

            return diffuseColor;
        }
    }
}
