﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace lab6
{
    class Point
    {
        public double X { get; set; }
        public double Y { get; set; }
        public double Z { get; set; }

        public double U { get; set; }
        public double V { get; set; }


        public Color Color { get; set; }

        public Vector Normal { get; set; }

        public Point(double x, double y, double z, double u = 0, double v = 0)
        {
            X = x; Y = y; Z = z;
            U = u; V = v;
            // Инициализируем вектор нормали
            Normal = new Vector(3);
        }

        public Point(Point p)
        {
           
            X = p.X; Y = p.Y; Z = p.Z;
            Normal = p.Normal;
        }

        public static double Distance(Point p1, Point p2)
        {
            double deltaX = p2.X - p1.X;
            double deltaY = p2.Y - p1.Y;
            double deltaZ = p2.Z - p1.Z;

            return Math.Sqrt(deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + X.GetHashCode();
                hash = hash * 23 + Y.GetHashCode();
                hash = hash * 23 + Z.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Point other = (Point)obj;
            return X.Equals(other.X) && Y.Equals(other.Y) && Z.Equals(other.Z);
        }

        static public void Swap(Point p1, Point p2)
        {
            var p2X = p2.X;
            var p2Y = p2.Y;
            var p2Z = p2.Z;

            p2.X = p1.X;
            p2.Y = p1.Y;
            p2.Z = p1.Z;
            p1.X = p2X;
            p1.Y = p2Y;
            p1.Z = p2Z;
        }

        public override string ToString()
        {
            return $"Point({{{Math.Truncate(X * 100) / 100};{Math.Truncate(Y * 100) / 100};{Math.Truncate(Z * 100) / 100}}})".Replace(',','.');
        }
    }

    class Edge
    {
        public Point P1 { get; set; }
        public Point P2 { get; set; }

        public Edge(Edge edge)
        {
            P1 = new Point(edge.P1);
            P2 = new Point(edge.P2);
        }

        public Edge(Point p1, Point p2)
        {
            P1 = p1; P2 = p2;
        }

        public Edge(double x1, double y1, double z1, double x2, double y2, double z2)
        {
            P1 = new Point(x1, y1, z1);
            P2 = new Point(x2, y2, z2);
        }

        public double GetLength()
        {
            return Point.Distance(P1, P2);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hash = 17;
                hash = hash * 23 + P1.GetHashCode();
                hash = hash * 23 + P2.GetHashCode();
                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Edge other = (Edge)obj;
            return P1.Equals(other.P1) && P2.Equals(other.P2);
        }
    }

    class Polygon
    {
        public List<Edge> Edges { get; set; }
        public Color Color { get; set; }

        public List<Point> Points { 
            get 
            {
                var list = new List<Point>();
                foreach (Edge e in Edges)
                {
                    if (!list.Contains(e.P1)) list.Add(e.P1);
                    if (!list.Contains(e.P2)) list.Add(e.P2);
                }

                return list;
            } 
        }

        public Polygon()
        {
            Edges = new List<Edge>();
        }

        public Polygon(List<Edge> edges)
        {
            Edges = edges;
        }

        public void AddEdge(Edge e)
        {
            Edges.Add(e);
        }

        public Point GetCenter()
        {
            var cx = 0.0;
            var cy = 0.0;
            var cz = 0.0;

            Points.ForEach(p =>
            {
                cx += p.X;
                cy += p.Y;
                cz += p.Z;
            });

            return new Point(cx / Points.Count, cy / Points.Count, cz / Points.Count);
        }

        public Vector GetNormal()
        {
            var points = Points;
            var u = new Vector(3, points[1].X - points[0].X, points[1].Y - points[0].Y, points[1].Z - points[0].Z);
            var v = new Vector(3, points[2].X - points[0].X, points[2].Y - points[0].Y, points[2].Z - points[0].Z);

            var normal = Vector.Cross3(v, u);

            normal.Normalize();

            return normal;
        }

        public override string ToString()
        {
            var str = "Polygon(";

            for (int i = 0; i < Points.Count; i++)
            {
                str = str + Points[i].ToString() + ((i < Points.Count - 1) ? "," : ")");
            }

            return str;
        }
    }

    class Polyhedron
    {
        static int ids = 1;

        public int Id { get; set; }
        public string Name { get; set; }
        public string NameId { get { return $"{Name} #{Id}"; } }

        public System.Drawing.Bitmap Texture { get; set; }

        double[] translation = new double[3];
        double[] rotation = new double[3];
        double[] scale = new double[3] { 1, 1, 1 };

        public List<Polygon> Polygons { get; set; }
        public double[] Translation { get { return translation; } }
        public double[] Rotation { get { return rotation; } }
        public double[] Scalation { get { return scale; } }

        public Polyhedron()
        {
            Polygons = new List<Polygon>();
            Id = ids++;
            Texture = null;
        }

        public Polyhedron(List<Polygon> polygons)
        {
            Polygons = polygons;
            Id = ids++;
            Texture = null;
        }

        public void AddPolygon(Polygon polygon)
        {
            Polygons.Add(polygon);
        }

        public void Translate(double deltaX, double deltaY, double deltaZ)
        {
            translation[0] += deltaX;
            translation[1] += deltaY;
            translation[2] += deltaZ;
        }

        public void Rotate(string axis, double angle)
        {
            switch (axis.ToLower())
            {
                case "x":
                    rotation[0] += angle;
                    break;
                case "y":
                    rotation[1] += angle;
                    break;
                case "z":
                    rotation[2] += angle;
                    break;
            }
        }

        public void Mirror(string axis)
        {
            switch (axis.ToLower())
            {
                case "x":
                    rotation[0] = Math.PI - rotation[0];
                    break;
                case "y":
                    rotation[1] = Math.PI - rotation[1];
                    break;
                case "z":
                    rotation[2] = Math.PI - rotation[2];
                    break;
            }
        }

        public void Scale(double cx, double cy, double cz)
        {
            scale[0] *= cx;
            scale[1] *= cy;
            scale[2] *= cz;
        }

        public Point GetCenter()
        {
            double x = 0;
            double y = 0;
            double z = 0;
            int count = 0;

            Polygons.ForEach(p => p.Edges.ForEach(e =>
            {
                x += e.P1.X + e.P2.X;
                y += e.P1.Y + e.P2.Y;
                z += e.P1.Z + e.P2.Z;
                count += 2;
            }));

            return new Point(x / count, y / count, z / count);
        }
    }


}
