﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6
{
    class CameraLookAt
    {
        double speedTranslation = 2;
        double speedRotation = Math.PI / 5;

        Vector cameraPosition = new Vector(3, 0, 0, 0);
        Vector cameraFront = new Vector(3, 0, 0, -1);
        Vector cameraUp = new Vector(3, 0, 1, 0);

        public Vector CameraPosition
        {
            get { return cameraPosition; }
        }

        double pitch = 0;
        double yaw = 0;

        public CameraLookAt() { 
            
        }

        public Vector GetCameraFront()
        {
            var rotationMatrix = Matrix.Identity();

            rotationMatrix.RotateX(pitch);
            rotationMatrix.RotateY(yaw);

            return Vector.TransformMat4(new Vector(3, 0, 0, -1), rotationMatrix);
        }

        public Matrix GetPerspectiveMatrix(double fovy, double aspect, double near, double far)
        {
            var f = 1.0 / Math.Tan(fovy / 2);
            var nf = 1.0 / (near - far);

            var matr = new Matrix(4, 4, f / aspect, 0, 0, 0,
                                    0, f, 0, 0,
                                    0, 0, (far + near) * nf, -1,
                                    0, 0, 2 * far * near * nf, 0);

            matr.Scale(-1, -1, 1);

            return matr;
        }

        public Matrix GetOrthoMatrix(double left, double right, double bottom, double top, double near, double far)
        {
            var lr = 1.0 / (left - right);
            var bt = 1.0 / (bottom - top);
            var nf = 1.0 / (near - far);
            var matr = new Matrix(4, 4, -2 * lr, 0, 0, 0,
                                        0, -2 * bt, 0, 0,
                                        0, 0, 2 * nf, 0,
                                        (left + right) * lr, (top + bottom) * bt, (far + near) * nf, 1);

            return matr;
        }

        public Matrix GetViewMatrix()
        {
            cameraFront = GetCameraFront();

            var cameraMatrix = LookAt(cameraPosition,
                Vector.Add(cameraPosition, cameraFront),
                cameraUp);

            return cameraMatrix;
        }

        public void Forward()
        {
            cameraPosition = Vector.ScaleAndAdd(cameraPosition, cameraFront, -speedTranslation);
        }

        public void Backward()
        {
            cameraPosition = Vector.ScaleAndAdd(cameraPosition, cameraFront, speedTranslation);
        }

        public void Up()
        {
            cameraPosition = Vector.ScaleAndAdd(cameraPosition, cameraUp, speedTranslation);
        }

        public void Down()
        {
            cameraPosition = Vector.ScaleAndAdd(cameraPosition, cameraUp, -speedTranslation);
        }

        public void Left()
        {
            var cameraRight = Vector.Cross3(cameraFront, cameraUp);
            cameraPosition = Vector.ScaleAndAdd(cameraPosition, cameraRight, -speedTranslation);
        }

        public void Right()
        {
            var cameraRight = Vector.Cross3(cameraFront, cameraUp);
            cameraPosition = Vector.ScaleAndAdd(cameraPosition, cameraRight, speedTranslation);
        }

        public void RotateUp()
        {
            pitch -= speedRotation;
        }

        public void RotateDown()
        {
            pitch += speedRotation;
        }

        public void RotateLeft()
        {
            yaw -= speedRotation;
        }

        public void RotateRight()
        {
            yaw += speedRotation;
        }


        private Matrix LookAt(Vector eye, Vector center, Vector up)
        {
            var eyex = eye[0];
            var eyey = eye[1];
            var eyez = eye[2];

            var upx = up[0];
            var upy = up[1];
            var upz = up[2];

            var centerx = center[0];
            var centery = center[1];
            var centerz = center[2];

            var eps = 0.000001;

            if (Math.Abs(eyex - centerx) < eps &&
                Math.Abs(eyey - centery) < eps &&
                Math.Abs(eyez - centerz) < eps)
            {

                return Matrix.Identity();

            }

            var z0 = eyex - centerx;
            var z1 = eyey - centery;
            var z2 = eyez - centerz;
            var len = 1 / Utils.Hypot(z0, z1, z2);

            z0 *= len;
            z1 *= len;
            z2 *= len;

            var x0 = upy * z2 - upz * z1;
            var x1 = upz * z0 - upx * z2;
            var x2 = upx * z1 - upy * z0;

            len = Utils.Hypot(x0, x1, x2);

            if (len == 0)
            {
                x0 = 0;
                x1 = 0;
                x2 = 0;

            }
            else
            {
                len = 1 / len;
                x0 *= len;
                x1 *= len;
                x2 *= len;
            }

            var y0 = z1 * x2 - z2 * x1;
            var y1 = z2 * x0 - z0 * x2;
            var y2 = z0 * x1 - z1 * x0;

            len = Utils.Hypot(y0, y1, y2);

            if (len == 0)
            {
                y0 = 0;
                y1 = 0;
                y2 = 0;
            }
            else
            {
                len = 1 / len;
                y0 *= len;
                y1 *= len;
                y2 *= len;
            }

            var matr = new Matrix(4, 4, x0, y0, z0, 0,
                                        x1, y1, z1, 0,
                                        x2, y2, z2, 0,
                                        -(x0 * eyex + x1 * eyey + x2 * eyez),
                                        -(y0 * eyex + y1 * eyey + y2 * eyez),
                                        -(z0 * eyex + z1 * eyey + z2 * eyez),
                                        1);

            return matr;
        }
    }
}
