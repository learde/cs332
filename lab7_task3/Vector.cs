﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6
{
     internal class Vector
    {
        double[] vect;

        public Vector(int n)
        {
            vect = new double[n];
        }

        public Vector(int n, params double[] values)
        {
            vect = new double[n];

            for (int i = 0; i < n; i++)
            {
                vect[i] = values[i];
            }
        }

        public int Length
        {
            get { return vect.Length; }
        }

        public double this[int i]
        {
            get { return vect[i]; }
            set { vect[i] = value; }
        }

        public double X { get { return vect[0]; } }
        public double Y { get { return vect[1]; } }
        public double Z { get { return vect[2]; } }
        public double W { get { return vect[3]; } }

       

        public override string ToString()
        {
            var str = "(";

            for (int i = 0; i < vect.Length ; i++)
            {
                if (i < vect.Length - 1) str = str + $"{vect[i]}, ";
                else str = str += $"{vect[i]})";
            }

            return str;
        }

        static public Vector Add(Vector a, Vector b)
        {
            var res = new Vector(a.Length);

            for (int i = 0; i < a.Length; i++)
            {
                res[i] = a[i] + b[i];
            }

            return res;
        }

        static public Vector ScaleAndAdd(Vector a, Vector b, double scale)
        {
            var res = new Vector(a.Length);

            for (int i = 0; i < a.Length; i++)
            {
                res[i] = a[i] + b[i] * scale;
            }

            return res;
        }

        static public Vector Cross3(Vector a, Vector b)
        {
            var res = new Vector(3);

            res[0] = a[1] * b[2] - a[2] * b[1];
            res[1] = a[2] * b[0] - a[0] * b[2];
            res[2] = a[0] * b[1] - a[1] * b[0];

            return res;

        }

        static public Vector TransformMat4(Vector a, Matrix m)
        {
            var x = a[0];
            var y = a[1];
            var z = a[2];
            var w = m[0, 3] * x + m[1, 3] * y + m[2, 3] * z + m[3, 3];

            if (w == 0) w = 1.0;

            var res = new Vector(3);

            res[0] = (m[0, 0] * x + m[1, 0] * y + m[2, 0] * z + m[3, 0]) / w;
            res[1] = (m[0, 1] * x + m[1, 1] * y + m[2, 1] * z + m[3, 1]) / w;
            res[2] = (m[0, 2] * x + m[1, 2] * y + m[2, 2] * z + m[3, 2]) / w;

            return res;
        }

        static public Vector FromPoint(Point p)
        {
            return new Vector(4, p.X, p.Y, p.Z, 1);
        }

        static public Point ToPoint(Vector v)
        {
            return new Point(v[0] / v[3], v[1] / v[3], v[2] / v[3]);
        }

        static public double DotProduct(Vector a, Vector b)
        {
            if (a.Length != b.Length)
                throw new ArgumentException("Векторы должны быть одинаковой длины");

            double result = 0;
            for (int i = 0; i < a.Length; i++)
            {
                result += a[i] * b[i];
            }
            return result;
        }

        static double Magnitude(Vector v)
        {
            double sumOfSquares = 0;

            for (int i = 0; i < v.Length; i++)
            {
                sumOfSquares += v[i] * v[i];
            }

            return Math.Sqrt(sumOfSquares);
        }

        public void Normalize()
        {
            double magnitude = Magnitude(this);

            if (magnitude != 0)
            {
                for (int i = 0; i < Length; i++)
                {
                    vect[i] /= magnitude;
                }
            }
        }
        public Vector NormalizetoVector()
        {
            double magnitude = Magnitude(this);

            if (magnitude != 0)
            {
                Vector normalizedVector = new Vector(Length);

                for (int i = 0; i < Length; i++)
                {
                    normalizedVector[i] = vect[i] / magnitude;
                }

                return normalizedVector;
            }

            return new Vector(Length); // Возвращаем вектор нулевой длины, если исходный вектор нулевой длины
        }

        static public double Angle(Vector a, Vector b)
        {
            double dotProduct = DotProduct(a, b);
            double magnitudeA = Magnitude(a);
            double magnitudeB = Magnitude(b);

            double cosTheta = dotProduct / (magnitudeA * magnitudeB);
            double angleRad = Math.Acos(cosTheta);

            Debug.WriteLine(a.ToString() + " " + b.ToString() + " " + angleRad + " " + DotProduct(a, b));

            return angleRad;
        }
    }
}
