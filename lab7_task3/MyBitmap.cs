﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace lab6
{
    class ColorTransforms
    {
        static public byte[] ToBgra(Color c)
        {
            return new byte[] { c.B, c.G, c.R, c.A };
        }

        static public byte[] ToBgra(System.Drawing.Color c)
        {
            return new byte[] { c.B, c.G, c.R, c.A };
        }

        static public Color Mix(Color c1, Color c2)
        {
            byte alpha = c1.A;
            byte invAlpha = (byte)(255 - alpha);

            byte newRed = (byte)((c1.R * alpha + c2.R * invAlpha) / 255);
            byte newGreen = (byte)((c1.G * alpha + c2.G * invAlpha) / 255);
            byte newBlue = (byte)((c1.B * alpha + c2.B * invAlpha) / 255);

            return Color.FromArgb(alpha, newRed, newGreen, newBlue);
        }
    }

    class MyColor
    {
        public double A;
        public double R;
        public double G;
        public double B;

        public Color Color => Color.FromArgb((byte)A, (byte)R, (byte)G, (byte)B);
        public byte[] ColorData => new byte[] { (byte)A, (byte)R, (byte)G, (byte)B };
        public MyColor(double R, double G, double B, double A = 255)
        {
            this.R = R;
            this.G = G;
            this.B = B;
            this.A = A;
        }

        public MyColor(Color c)
        {
            R = c.R;
            G = c.G;
            B = c.B;
            A = c.A;
        }

        static public MyColor Mult(MyColor c1, double k)
        {
            return new MyColor(c1.R * k, c1.G * k, c1.B * k);
        }

        static public MyColor Diff(MyColor c1, MyColor c2)
        {
            return new MyColor(c1.R - c2.R, c1.G - c2.G, c1.B - c2.B);
        }

        static public MyColor Sum(MyColor c1, MyColor c2)
        {
            return new MyColor(c1.R + c2.R, c1.G + c2.G, c1.B + c2.B);
        }

        static public void Swap(MyColor c1, MyColor c2)
        {
            var c1A = c1.A;
            var c1R = c1.R;
            var c1G = c1.G;
            var c1B = c1.B;

            c1.A = c2.A;
            c1.R = c2.R;
            c1.G = c2.G;
            c1.B = c2.B;

            c2.A = c1A;
            c2.R = c1R;
            c1.G = c1G;
            c1.B = c1B;
        }
    }

    class MyBitmap
    {
        WriteableBitmap bitmap;
        int width;
        int height;
        int stride;
        byte[] pixels;

        int shiftX;
        int shiftY;

        bool shouldDrawSmooth = false;

        double[,] zBuffer;
        bool isZBufferEnabled;

        public WriteableBitmap Bitmap { get { return bitmap; } }

        public MyBitmap(int width, int height)
        {
            this.width = width;
            this.height = height;
            zBuffer = new double[width, height];

            bitmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgra32, null);
            int pixelWidth = bitmap.PixelWidth;
            int pixelHeight = bitmap.PixelHeight;

            stride = pixelWidth * ((bitmap.Format.BitsPerPixel + 7) / 8);
            pixels = new byte[pixelHeight * stride];

            shiftX = width / 2;
            shiftY = height / 2;

            Clear();
        }

        public void EnableZBuffer(bool flag)
        {
            isZBufferEnabled = flag;
        }

        public void Clear()
        {
            var whiteBytes = new byte[width * height * 4];
            for (var i = 0; i < whiteBytes.Length; i += 4)
            {
                whiteBytes[i] = 255;
                whiteBytes[i + 1] = 255;
                whiteBytes[i + 2] = 255;
                whiteBytes[i + 3] = 0;
            }
            bitmap.WritePixels(new Int32Rect(0, 0, width, height), whiteBytes, width * 4, 0);
        }

        public void ClearZBuffer(double z)
        {
            for (int i = 0; i < zBuffer.GetLength(0); i++)
            {
                for (int j = 0; j < zBuffer.GetLength(1); j++)
                {
                    zBuffer[i, j] = z;
                }
            }
        }

        public Color GetPixel(int x, int y)
        {
            int index = y * stride + 4 * x;
            byte blue = pixels[index];
            byte green = pixels[index + 1];
            byte red = pixels[index + 2];
            byte alpha = pixels[index + 3];
            return Color.FromArgb(alpha, red, green, blue);
        }

        public void SetPixels()
        {
            int pixelWidth = bitmap.PixelWidth;
            int pixelHeight = bitmap.PixelHeight;
            bitmap.CopyPixels(new Int32Rect(0, 0, pixelWidth, pixelHeight), pixels, stride, 0);
        }

        public void FillTriangle(Point p1, Color c1, Point p2, Color c2, Point p3, Color c3)
        {
            Point np1 = new Point(0, 0, 0);
            Point np2 = new Point(p2.X - p1.X, p2.Y - p1.Y, 0);
            Point np3 = new Point(p3.X - p1.X, p3.Y - p1.Y, 0);

            MyColor mc1 = new MyColor(c1);
            MyColor mc2 = new MyColor(c2);
            MyColor mc3 = new MyColor(c3);

            if (np3.Y == 0)
            {
                Point.Swap(np2, np3);
                MyColor.Swap(mc2, mc3);
            }

            MyColor diff1 = MyColor.Diff(mc2, mc1);
            MyColor diff2 = MyColor.Diff(mc3, mc1);

            int xMin = (int)Math.Floor(Math.Min(np1.X, Math.Min(np2.X, np3.X)))-1;
            int yMin = (int)Math.Floor(Math.Min(np1.Y, Math.Min(np2.Y, np3.Y)))-1;
            int xMax = (int)Math.Ceiling(Math.Max(np1.X, Math.Max(np2.X, np3.X)))+1;
            int yMax = (int)Math.Ceiling(Math.Max(np1.Y, Math.Max(np2.Y, np3.Y)))+1;
            double eps = 0.00;

            double a = p1.Y * (p2.Z - p3.Z) + p2.Y * (p3.Z - p1.Z) + p3.Y * (p1.Z - p2.Z);
            double b = p1.Z * (p2.X - p3.X) + p2.Z * (p3.X - p1.X) + p3.Z * (p1.X - p2.X);
            double c = p1.X * (p2.Y - p3.Y) + p2.X * (p3.Y - p1.Y) + p3.X * (p1.Y - p2.Y);
            double d = -(p1.X * (p2.Y * p3.Z - p3.Y * p2.Z) + p2.X * (p3.Y * p1.Z - p1.Y * p3.Z) + p3.X * (p1.Y * p2.Z - p2.Y * p1.Z));

            for (int y = yMin; y <= yMax; y++)
            {
                for (int x = xMin; x <= xMax; x++)
                {
                    double w1 = (y * np3.X - x * np3.Y) / (np2.Y * np3.X - np2.X * np3.Y);

                    if (w1 + eps >= 0 && w1 - eps <= 1)
                    {
                        double w2 = (y - w1 * np2.Y) / np3.Y;

                        if (w2 + eps >= 0 && ((w1 + w2) - eps <= 1))
                        {

                            MyColor diff1w1 = MyColor.Mult(diff1, w1);
                            MyColor diff2w2 = MyColor.Mult(diff2, w2);

                            Color resultedColor = MyColor.Sum(MyColor.Sum(mc1, diff1w1), diff2w2).Color;

                            double resX = x + p1.X;
                            double resY = y + p1.Y;
                            double resZ = -(a * resX + b * resY + d) / c;

                            DrawPoint(resX, resY, resZ, resultedColor);
                        }
                    }
                }
            }
        }

        public void FillTriangleTexture(Point p1, Point p2, Point p3, System.Drawing.Bitmap texture, bool isParallel)
        {
            Point np1 = new Point(0, 0, 0);
            Point np2 = new Point(p2.X - p1.X, p2.Y - p1.Y, 0);
            Point np3 = new Point(p3.X - p1.X, p3.Y - p1.Y, 0);

            var u1 = p1.U;
            var v1 = p1.V;
            var u2 = p2.U;
            var v2 = p2.V;
            var u3 = p3.U;
            var v3 = p3.V;

            bool shouldSwapW = true;

            if (np3.Y == 0)
            {
                Point.Swap(np2, np3);
                shouldSwapW = false;
            }

            int xMin = (int)Math.Floor(Math.Min(np1.X, Math.Min(np2.X, np3.X))) - 1;
            int yMin = (int)Math.Floor(Math.Min(np1.Y, Math.Min(np2.Y, np3.Y))) - 1;
            int xMax = (int)Math.Ceiling(Math.Max(np1.X, Math.Max(np2.X, np3.X))) + 1;
            int yMax = (int)Math.Ceiling(Math.Max(np1.Y, Math.Max(np2.Y, np3.Y))) + 1;

            double a = p1.Y * (p2.Z - p3.Z) + p2.Y * (p3.Z - p1.Z) + p3.Y * (p1.Z - p2.Z);
            double b = p1.Z * (p2.X - p3.X) + p2.Z * (p3.X - p1.X) + p3.Z * (p1.X - p2.X);
            double c = p1.X * (p2.Y - p3.Y) + p2.X * (p3.Y - p1.Y) + p3.X * (p1.Y - p2.Y);
            double d = -(p1.X * (p2.Y * p3.Z - p3.Y * p2.Z) + p2.X * (p3.Y * p1.Z - p1.Y * p3.Z) + p3.X * (p1.Y * p2.Z - p2.Y * p1.Z));

            for (int y = yMin; y <= yMax; y++)
            {
                for (int x = xMin; x <= xMax; x++)
                {
                    double w1 = (y * np3.X - x * np3.Y) / (np2.Y * np3.X - np2.X * np3.Y);

                    if (w1 >= 0 && w1 <= 1)
                    {
                        double w2 = (y - w1 * np2.Y) / np3.Y;

                        if (w2 >= 0 && ((w1 + w2) <= 1))
                        {
                            double resX = x + p1.X;
                            double resY = y + p1.Y;
                            double resZ = -(a * resX + b * resY + d) / c;

                            if (shouldSwapW)
                            {
                                (w1, w2) = (w2, w1);
                            }

                            int textX; int textY;

                            if (isParallel) {
                                textX = (int)(texture.Width * (u1 + (u3 - u1) * w1 + (u2 - u1) * w2));
                                textY = (int)(texture.Height * (v1 + (v3 - v1) * w1 + (v2 - v1) * w2));
                            } else
                            {
                                textX = (int)((texture.Width * (u1 / p1.Z + (u3 / p3.Z - u1 / p1.Z) * w1 + (u2 / p2.Z - u1 / p1.Z) * w2)) / (1 / p1.Z + (1 / p3.Z - 1 / p1.Z) * w1 + (1 / p2.Z - 1 / p1.Z) * w2));
                                textY = (int)((texture.Height * (v1 / p1.Z + (v3 / p3.Z - v1 / p1.Z) * w1 + (v2 / p2.Z - v1 / p1.Z) * w2)) / (1 / p1.Z + (1 / p3.Z - 1 / p1.Z) * w1 + (1 / p2.Z - 1 / p1.Z) * w2));
                            }

                            if (textX >= texture.Width) textX = texture.Width - 1;
                            if (textY >= texture.Height) textY = texture.Height - 1;

                            var resultedColor = texture.GetPixel(textX, textY);

                            DrawPoint(resX, resY, resZ, ColorTransforms.ToBgra(resultedColor));
                        }
                    }
                }
            }
        }

        private void DrawStraightLine(int x1, int y, int x2, Color color)
        {
            DrawStraightLine(x1, y, x2, ColorTransforms.ToBgra(color));
        }

        private void DrawStraightLine(int x1, int y, int x2, byte[] colorData)
        {
            int width = x2 - x1;

            byte[] pixels = new byte[width * 4];

            for (var i = 0; i < pixels.Length; i += 4)
            {
                pixels[i] = colorData[0];
                pixels[i + 1] = colorData[1];
                pixels[i + 2] = colorData[2];
                pixels[i + 3] = colorData[3];
            }

            int stride = width * 4;
            Int32Rect rect = new Int32Rect(x1, y, width, 1);
            bitmap.WritePixels(rect, pixels, stride, 0);
        }

        public void DrawLine(Edge edge, Color color, int thickness = 1)
        {
            DrawLine(edge.P1, edge.P2, color, thickness);
        }

        public void DrawLine(Point p1, Point p2, Color color, int thickness = 1)
        {

            DrawLine((int)p1.X, (int)p1.Y, (int)p1.Z, (int)p2.X, (int)p2.Y, (int)p2.Z, color, thickness);
        }

        public void DrawLine(int x1, int y1, int z1, int x2, int y2, int z2, Color color, int thickness = 1)
        {
            DrawLine(x1, y1, z1, x2, y2, z2, ColorTransforms.ToBgra(color), thickness);
        }

        public void DrawLine(double x1, double y1, double z1, double x2, double y2, double z2, Color color, int thickness = 1)
        {
            DrawLine((int)x1, (int)y1, (int)z1, (int)x2, (int)y2, (int)z2, ColorTransforms.ToBgra(color), thickness);
        }

        public void DrawLine(int x1, int y1, int z1, int x2, int y2, int z2, byte[] colorData, int thickness = 1)
        {
            DrawPoint(x1, y1, z1, colorData, thickness);

            int dx = Math.Abs(x2 - x1);
            int dy = Math.Abs(y2 - y1);
            int dz = Math.Abs(z2 - z1);
            int xs;
            int ys;
            int zs;
            if (x2 > x1)
                xs = 1;
            else
                xs = -1;
            if (y2 > y1)
                ys = 1;
            else
                ys = -1;
            if (z2 > z1)
                zs = 1;
            else
                zs = -1;

            // Driving axis is X-axis"
            if (dx >= dy && dx >= dz)
            {
                int p1 = 2 * dy - dx;
                int p2 = 2 * dz - dx;
                while (x1 != x2)
                {
                    x1 += xs;
                    if (p1 >= 0)
                    {
                        y1 += ys;
                        p1 -= 2 * dx;
                    }
                    if (p2 >= 0)
                    {
                        z1 += zs;
                        p2 -= 2 * dx;
                    }
                    p1 += 2 * dy;
                    p2 += 2 * dz;
                    DrawPoint(x1, y1, z1, colorData, thickness);
                }

                // Driving axis is Y-axis"
            }
            else if (dy >= dx && dy >= dz)
            {
                int p1 = 2 * dx - dy;
                int p2 = 2 * dz - dy;
                while (y1 != y2)
                {
                    y1 += ys;
                    if (p1 >= 0)
                    {
                        x1 += xs;
                        p1 -= 2 * dy;
                    }
                    if (p2 >= 0)
                    {
                        z1 += zs;
                        p2 -= 2 * dy;
                    }
                    p1 += 2 * dx;
                    p2 += 2 * dz;
                    DrawPoint(x1, y1, z1, colorData, thickness);
                }

                // Driving axis is Z-axis"
            }
            else
            {
                int p1 = 2 * dy - dz;
                int p2 = 2 * dx - dz;
                while (z1 != z2)
                {
                    z1 += zs;
                    if (p1 >= 0)
                    {
                        y1 += ys;
                        p1 -= 2 * dz;
                    }
                    if (p2 >= 0)
                    {
                        x1 += xs;
                        p2 -= 2 * dz;
                    }
                    p1 += 2 * dy;
                    p2 += 2 * dx;
                    DrawPoint(x1, y1, z1, colorData, thickness);
                }
            }
        }

        public void DrawPoint(int x, int y, int z, Color color, int thickness = 1)
        {
            DrawPoint(x, y, z, ColorTransforms.ToBgra(color), thickness);
        }

        public void DrawPoint(double x, double y, double z, Color color, int thickness = 1)
        {
            DrawPoint((int)x, (int)y, (int)z, ColorTransforms.ToBgra(color), thickness);
        }

        public void DrawPoint(double x, double y, double z, byte[] colorData, int thickness = 1)
        {
            DrawPoint((int)x, (int)y, (int)z, colorData, thickness);
        }

        public void DrawPoint(int x, int y, int z, byte[] colorData, int thickness = 1)
        {
            int radius = thickness / 2;

            for (int i = -radius; i <= radius; i++)
            {
                for (int j = -radius; j <= radius; j++)
                {
                    if (j * j + i * i <= radius * radius)
                    {
                        int pixelX = x + j + shiftX;
                        int pixelY = height - (y + i + shiftY);
                        if (pixelX >= 0 && pixelX < width && pixelY >= 0 && pixelY < height)
                        {
                            if (isZBufferEnabled)
                            {
                                if (zBuffer[pixelX, pixelY] < z)
                                {
                                    zBuffer[pixelX, pixelY] = z;
                                    Int32Rect rect = new Int32Rect(pixelX, pixelY, 1, 1);
                                    bitmap.WritePixels(rect, colorData, 4, 0);
                                }
                            } else
                            {
                                Int32Rect rect = new Int32Rect(pixelX, pixelY, 1, 1);
                                bitmap.WritePixels(rect, colorData, 4, 0);
                            }
                        }
                    }
                }
            }
        }
    }
}
