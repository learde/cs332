﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab6
{
    class Matrix
    {
        public double[,] Matr { get; set; }
        public int N { get; set; }
        public int M { get; set; }

        public Matrix(int n, int m)
        {
            Init(n, m);
        }

        public Matrix(int n, int m, params double[] values)
        {
            Init(n, m);

            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                {
                    Matr[i, j] = values[i * m + j];
                }

        }

        public Matrix(int n, int m, double[,] matr)
        {
            Matr = matr;
            N = n;
            M = m;
        }

        public double this[int i, int j]
        {
            get { return Matr[i, j]; }
            set { Matr[i, j] = value; }
        }

        public Matrix GetMultBy(Matrix m)
        {
            if (M != m.N)
            {
                throw new InvalidOperationException("Умножение матриц невозможно: количество столбцов первой матрицы не равно количеству строк второй матрицы.");
            }

            Matrix result = new Matrix(N, m.M);

            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < m.M; j++)
                {
                    double sum = 0;

                    for (int k = 0; k < M; k++)
                    {
                        sum += Matr[i, k] * m.Matr[k, j];
                    }

                    result.Matr[i, j] = sum;
                }
            }

            return result;
        }

        public void Translate(double x, double y, double z)
        {
            Matrix matr = new Matrix(4, 4, 1, 0, 0, 0,
                                           0, 1, 0, 0,
                                           0, 0, 1, 0,
                                           x, y, z, 1);

            Multiply(matr);

            //Matr[3, 0] = Matr[0, 0] * x + Matr[1, 0] * y + Matr[2, 0] * z + Matr[3, 0];
            //Matr[3, 1] = Matr[0, 1] * x + Matr[1, 1] * y + Matr[2, 1] * z + Matr[3, 1];
            //Matr[3, 2] = Matr[0, 2] * x + Matr[1, 2] * y + Matr[2, 2] * z + Matr[3, 2];
            //Matr[3, 3] = Matr[0, 3] * x + Matr[1, 3] * y + Matr[2, 3] * z + Matr[3, 3];
        }

        public void RotateX(double angle)
        {
            Matrix matr = new Matrix(4, 4, 1, 0, 0, 0,
                                    0, Utils.Cos(angle), Utils.Sin(angle), 0,
                                    0, -Utils.Sin(angle), Utils.Cos(angle), 0,
                                    0, 0, 0, 1);

            Multiply(matr);
        }

        public void RotateY(double angle)
        {
            Matrix matr = new Matrix(4, 4, Utils.Cos(angle), 0, -Utils.Sin(angle), 0,
                                    0, 1, 0, 0,
                                    Utils.Sin(angle), 0, Utils.Cos(angle), 0,
                                    0, 0, 0, 1);

            Multiply(matr);
        }

        public void RotateZ(double angle)
        {
            Matrix matr = new Matrix(4, 4, Utils.Cos(angle), Utils.Sin(angle), 0, 0,
                                    -Utils.Sin(angle), Utils.Cos(angle), 0, 0,
                                    0, 0, 1, 0,
                                    0, 0, 0, 1);

            Multiply(matr);
        }

        public Matrix Scale(double cx, double cy, double cz)
        {
            Matrix matr = new Matrix(4, 4, cx, 0, 0, 0,
                                    0, cy, 0, 0,
                                    0, 0, cz, 0,
                                    0, 0, 0, 1);

            Multiply(matr);

            return this;
        }

        static public Matrix Multiply(Matrix m1, Matrix m2)
        {
            return m1.GetMultBy(m2);
        }

        public void Multiply(Matrix m)
        {
            Matr = GetMultBy(m).Matr;
        }

        public static Vector Multiply(Matrix m, Vector v)
        {
            if (m.M != v.Length)
                throw new ArgumentException("Умножение матриц невозможно: количество столбцов первой матрицы не равно количеству строк второй матрицы.");

            Vector result = new Vector(m.M);

            for (int i = 0; i < m.N; i++)
            {
                double sum = 0.0;
                for (int j = 0; j < m.M; j++)
                {
                    sum += m[i, j] * v[j];
                }
                result[i] = sum;
            }

            return result;
        }

        public void ToIdentity()
        {
            Matr = Identity().Matr;
        }

        static public Matrix Identity()
        {
            return new Matrix(4, 4, 1, 0, 0, 0,
                                    0, 1, 0, 0,
                                    0, 0, 1, 0,
                                    0, 0, 0, 1);
        }

        public void ToInvert()
        {
            Matr = Invert(this).Matr;
        }

        static public Matrix Invert(Matrix a)
        {
            var a00 = a[0, 0];
            var a01 = a[0, 1];
            var a02 = a[0, 2];
            var a03 = a[0, 3];

            var a10 = a[1, 0];
            var a11 = a[1, 1];
            var a12 = a[1, 2];
            var a13 = a[1, 3];

            var a20 = a[2, 0];
            var a21 = a[2, 1];
            var a22 = a[2, 2];
            var a23 = a[2, 3];

            var a30 = a[3, 0];
            var a31 = a[3, 1];
            var a32 = a[3, 2];
            var a33 = a[3, 3];

            var b00 = a00 * a11 - a01 * a10;
            var b01 = a00 * a12 - a02 * a10;
            var b02 = a00 * a13 - a03 * a10;
            var b03 = a01 * a12 - a02 * a11;
            var b04 = a01 * a13 - a03 * a11;
            var b05 = a02 * a13 - a03 * a12;
            var b06 = a20 * a31 - a21 * a30;
            var b07 = a20 * a32 - a22 * a30;
            var b08 = a20 * a33 - a23 * a30;
            var b09 = a21 * a32 - a22 * a31;
            var b10 = a21 * a33 - a23 * a31;
            var b11 = a22 * a33 - a23 * a32;

            var det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

            if (det == 0)
            {
                return Identity();
            }

            det = 1.0 / det;

            var res = new Matrix(4, 4);

            res[0, 0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
            res[0, 1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
            res[0, 2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
            res[0, 3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
            res[1, 0] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
            res[1, 1] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
            res[1, 2] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
            res[1, 3] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
            res[2, 0] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
            res[2, 1] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
            res[2, 2] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
            res[2, 3] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
            res[3, 0] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
            res[3, 1] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
            res[3, 2] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
            res[3, 3] = (a20 * b03 - a21 * b01 + a22 * b00) * det;

            return res;
        }

        static public Matrix Transpose(Matrix m)
        {
            var res = new Matrix(4, 4);
            res[0,0] = m[0,0];
            res[0,1] = m[1,0];
            res[0,2] = m[2,0];
            res[0,3] = m[3,0];
            res[1,0] = m[0,1];
            res[1,1] = m[1,1];
            res[1,2] = m[2,1];
            res[1,3] = m[3,1];
            res[2,0] = m[0,2];
            res[2,1] = m[1,2];
            res[2,2] = m[2,2];
            res[2,3] = m[3,2];
            res[3,0] = m[0,3];
            res[3,1] = m[1,3];
            res[3,2] = m[2,3];
            res[3,3] = m[3,3];

            return res;
        }

        private void Init(int n, int m)
        {
            Matr = new double[n, m];
            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                {
                    Matr[i, j] = 0;
                }
            N = n;
            M = m;
        }

        static public Matrix FromPoint(Point p, double shift = 0)
        {
            return new Matrix(1, 4, p.X + shift, p.Y + shift, p.Z + shift, 1);
        }

        static public Matrix FromVector(Vector v, double shift = 0)
        {
            return new Matrix(1, 4, v[0] + shift, v[1] + shift, v[2] + shift, 1);
        }

        static public Point ToPoint(Matrix m)
        {
            var w = m[0, 3];
            if (w == 0) w = 1.0;
            return new Point(m[0, 0] / w, m[0, 1] / w, m[0, 2]);
        }

        static public Vector ToVector(Matrix m)
        {
            var w = m[0, 3];
            if (w == 0) w = 1.0;
            return new Vector(3, m[0, 0] / w, m[0, 1] / w, m[0, 2]);
        }
    }
}
