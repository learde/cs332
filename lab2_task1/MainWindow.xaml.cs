﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Interop;

namespace lab2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Bitmap bitmapGray1;
        private Bitmap bitmapGray2;

        private int diffCoeff = 1;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void fillHistogram(Canvas canvas, Bitmap bmp)
        {
            canvas.Children.Clear();
            var widthRect = canvas.ActualWidth / 256;
            var intensities = new int[256];

            System.Drawing.Rectangle rectBmp = new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height);
            BitmapData bmpData =
                bmp.LockBits(rectBmp, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                bmp.PixelFormat);

            IntPtr ptr = bmpData.Scan0;

            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];

            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            for (int counter = 0; counter + 2 < rgbValues.Length; counter += 3)
            {
                intensities[rgbValues[counter]]++;
            }

            bmp.UnlockBits(bmpData);

            var maxIntensity = intensities.Max();
            var coeff = canvas.ActualHeight / maxIntensity;
            var correctedIntensities = intensities.Select(i => (i * coeff)).ToArray();

            for (int i = 0; i < correctedIntensities.Length; i++)
            {
                var rect = new System.Windows.Shapes.Rectangle();
                rect.Width = widthRect;
                rect.Height = correctedIntensities[i];
                rect.Fill = System.Windows.Media.Brushes.BlueViolet;
                Canvas.SetTop(rect, canvas.ActualHeight - rect.Height);
                Canvas.SetLeft(rect, i * widthRect);
                canvas.Children.Add(rect);
            }
        }

        private byte pixelToGray_YUV_YIQ(byte red, byte green, byte blue)
        {
            return Convert.ToByte(0.299 * red + 0.587 * green + 0.114 * blue);
        }
        private byte pixelToGray_HDTV(byte red, byte green, byte blue)
        {
            return Convert.ToByte(0.2126 * red + 0.7152 * green + 0.0722 * blue);
        }

        private BitmapSource imageToGray_YUV_YIQ(string path)
        {
            Bitmap bmp = new Bitmap(path);
            bitmapGray1 = imageToGray(bmp, pixelToGray_YUV_YIQ);
            fillHistogram(canvas1, bitmapGray1);
            return bitmapToBitmapSource(bitmapGray1);
        }

        private BitmapSource imageToGray_HDTV(string path)
        {
            Bitmap bmp = new Bitmap(path);
            bitmapGray2 = imageToGray(bmp, pixelToGray_HDTV);
            fillHistogram(canvas2, bitmapGray2);
            return bitmapToBitmapSource(bitmapGray2);
        }

        private BitmapSource bitmapToBitmapSource(Bitmap bmp)
        {
            return Imaging.CreateBitmapSourceFromHBitmap(bmp.GetHbitmap(), IntPtr.Zero, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
        }

        private Bitmap imageToGray(Bitmap bmpSource, Func<byte, byte, byte, byte> pixelToGray)
        {
            Bitmap bmp = (Bitmap) bmpSource.Clone();

            System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height);
            BitmapData bmpData =
                bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                bmp.PixelFormat);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);

            // Set every third value to 255. A 24bpp bitmap will look red.  
            for (int counter = 0; counter + 2 < rgbValues.Length; counter += 3)
            {
                var blue = rgbValues[counter];
                var green = rgbValues[counter + 1];
                var red = rgbValues[counter + 2];
                byte resultedPixel = pixelToGray(red, green, blue);

                rgbValues[counter] = resultedPixel; // blue
                rgbValues[counter + 1] = resultedPixel; // green
                rgbValues[counter + 2] = resultedPixel; // red
            }

            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            bmp.UnlockBits(bmpData);

            return bmp;
        }

        private BitmapSource getDiffGrayImages(Bitmap src1, Bitmap src2)
        {
            Bitmap bmp = (Bitmap) src1.Clone();
            Bitmap bmp2 = (Bitmap) src2.Clone();

            System.Drawing.Rectangle rect = new System.Drawing.Rectangle(0, 0, bmp.Width, bmp.Height);
            System.Drawing.Rectangle rect2 = new System.Drawing.Rectangle(0, 0, bmp2.Width, bmp2.Height);
            BitmapData bmpData =
                bmp.LockBits(rect, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                bmp.PixelFormat);
            BitmapData bmpData2 =
                bmp2.LockBits(rect2, System.Drawing.Imaging.ImageLockMode.ReadWrite,
                bmp2.PixelFormat);

            // Get the address of the first line.
            IntPtr ptr = bmpData.Scan0;
            IntPtr ptr2 = bmpData2.Scan0;

            // Declare an array to hold the bytes of the bitmap.
            int bytes = Math.Abs(bmpData.Stride) * bmp.Height;
            byte[] rgbValues = new byte[bytes];
            byte[] rgbValues2 = new byte[bytes];

            // Copy the RGB values into the array.
            System.Runtime.InteropServices.Marshal.Copy(ptr, rgbValues, 0, bytes);
            System.Runtime.InteropServices.Marshal.Copy(ptr2, rgbValues2, 0, bytes);

            // Set every third value to 255. A 24bpp bitmap will look red.  
            for (int counter = 0; counter < rgbValues.Length; counter += 1)
            {
                int c1 = rgbValues[counter];
                int c2 = rgbValues2[counter];

                rgbValues[counter] = Convert.ToByte(Math.Abs(c1 - c2) * diffCoeff);
            }

            // Copy the RGB values back to the bitmap
            System.Runtime.InteropServices.Marshal.Copy(rgbValues, 0, ptr, bytes);

            // Unlock the bits.
            bmp.UnlockBits(bmpData);

            return bitmapToBitmapSource(bmp);
        }

        private void fillImages(string path)
        {
            initialImage.Source = new BitmapImage(new Uri(path));
            grayImage1.Source = imageToGray_YUV_YIQ(path);
            grayImage2.Source = imageToGray_HDTV(path);
            diffImage.Source = getDiffGrayImages(bitmapGray1, bitmapGray2);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new Microsoft.Win32.OpenFileDialog();
            openFileDialog.Filter = "Изображения|*.jpeg;*.jpg";

            if (openFileDialog.ShowDialog() == true)
            {
                fillImages(openFileDialog.FileName);
            }
        }
    }
}
